﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace Min_Mang {

  class LidskyHrac : Hrac {
    public override void vratTah(GUI grafika, HerniBarva kdoTahne, EventWaitHandle value, int hloubka) {
      grafika.NastavEWH(value);
      grafika.NastavPovoleneTahnuti(true);
      value.WaitOne();
    }
  }
}
