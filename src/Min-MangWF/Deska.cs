﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Min_Mang {
  public class StavHraciDesky {

    /// <summary>
    /// Reprezentace herní desky pomocí stavů polí
    /// </summary>
    private HerniBarva[] stav;

    /// <summary>
    /// Konstruktor vytvářející počáteční stav (rozestavení) desky
    /// </summary>
    public StavHraciDesky() {
      stav = new HerniBarva[81];
      for (int i = 0; i < 81; i++) {
        if (i % 9 == 0)
          stav[i] = HerniBarva.BILY;
        else if (i % 9 == 8)
          stav[i] = HerniBarva.CERNY;
        else if (i / 9 == 8)
          stav[i] = HerniBarva.CERNY;
        else if (i / 9 == 0)
          stav[i] = HerniBarva.BILY;
        else
          stav[i] = HerniBarva.PRAZDNY;
      }
    }
    // testovaci verze desky ////////////////////
    public StavHraciDesky(string s) {
      stav = new HerniBarva[81];
      for(int i =0;i<81;i++) {
        stav[i] = HerniBarva.PRAZDNY;
      }
      stav[3] = HerniBarva.BILY;
      stav[12] = HerniBarva.BILY;
      stav[11] = HerniBarva.BILY;
      stav[10] = HerniBarva.BILY;
      stav[1] = HerniBarva.CERNY;
    }

    public StavHraciDesky(StavHraciDesky puvodniDeska) {
      stav = new HerniBarva[81];
      for (int i = 0; i < 81; i++)
        stav[i] = puvodniDeska.stav[i];
      foreach (Tah t in puvodniDeska.historieTahu) {
        this.PridejTahDoHistorie(t);
      }
      
    }


    /// <summary>
    /// Vrací hodnotu herního pole
    /// </summary>
    /// <param name="index">Číslo políčka, které nás zajímá</param>
    /// <returns>Stav pole, o které jsme se zajímali, vyjádřený enumem HerniBarva</returns>
    public HerniBarva VratHodnotuPole(int index) {
      return stav[index];
    }

    private void NastavHonotuPole(int index, HerniBarva barva) {
      stav[index] = barva;
    }

    public StavHraciDesky ProvedTah(Tah t) {
      this.NastavHonotuPole(t.Kam, this.VratHodnotuPole(t.Odkud));
      this.NastavHonotuPole(t.Odkud, HerniBarva.PRAZDNY);

      return this;
    }


    /// <summary>
    /// Vrací provedený tah zpět
    /// </summary>
    /// <param name="t">Tah</param>
    /// <returns>Hrací deska</returns>
    public StavHraciDesky ReverseTah(Tah t) {
      this.NastavHonotuPole(t.Odkud, this.VratHodnotuPole(t.Kam));
      this.NastavHonotuPole(t.Kam, HerniBarva.PRAZDNY);

      return this;
    }


    public void VyhodKameny(int[] pole) {
      if (pole.Length > 0) {
        foreach (int index in pole) {
          this.NastavHonotuPole(index, HerniBarva.PRAZDNY);
        }
      }
    }

    public void ReverseVyhodKameny(int[] pole, HerniBarva barva) {
      barva = (barva == HerniBarva.BILY ? HerniBarva.CERNY : HerniBarva.BILY);
      if (pole.Length > 0) {
        foreach (int index in pole) {
          this.NastavHonotuPole(index, barva);
        }
      }
    }

    /// <summary>
    /// Spočítá kameny určité barvy na desce
    /// </summary>
    /// <param name="barva">Barva hráče, pro kterého se kameny počítjí</param>
    /// <returns>Počet kamenů</returns>
    public int SpocitejKameny(HerniBarva barva) {
      return stav.Count(i => i==barva);
    }

    List<Tah> historieTahu = new List<Tah>();

    public List<Tah> HistorieTahu {
      get { return historieTahu; }
      set { historieTahu = value; }
    }

    public void PridejTahDoHistorie(Tah t) {
      HistorieTahu.Add(t);
    }



  }
}