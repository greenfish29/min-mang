﻿using System;
using System.Windows.Forms;
using System.Threading;

namespace Min_Mang {
  partial class NovaHra : Form {

    private GUI hlavniOkno;
    public event ZmenaStavuHandler StartNovaHra;
    public event ZmenaStavuHandler ZmenaNastaveniHracu;
    public bool ZmenaHracu { get; set; }
    private EventWaitHandle ewh;

    public NovaHra(GUI gui) {
      InitializeComponent();
      hlavniOkno = gui;
      ewh = hlavniOkno.ewh;
    }

    private void btnOk_Click(object sender, EventArgs e) {
      hlavniOkno.kresliKameny = true;

      #region nastaveni radiobuttonu
      // typ hrace 1
      if (rbHrac1Clovek.Checked)
        hlavniOkno.typHrace1 = TypHrace.Clovek;
      else
        hlavniOkno.typHrace1=TypHrace.Pocitac;
      // typ hrace 2
      if (rbHrac2Clovek.Checked)
        hlavniOkno.typHrace2=TypHrace.Clovek;
      else
        hlavniOkno.typHrace2=TypHrace.Pocitac;

      // obtiznost hrace 1
      if(rbHrac1Lehka.Checked)
        hlavniOkno.obtiznostH1=Obtiznost.Lehka;
      else if(rbHrac1Stredni.Checked)
        hlavniOkno.obtiznostH1=Obtiznost.Stredni;
      else
        hlavniOkno.obtiznostH1=Obtiznost.Tezka;

      // obtiznost hrace 2
      if (rbHrac2Lehka.Checked)
        hlavniOkno.obtiznostH2 = Obtiznost.Lehka;
      else if (rbHrac2Stredni.Checked)
        hlavniOkno.obtiznostH2 = Obtiznost.Stredni;
      else
        hlavniOkno.obtiznostH2 = Obtiznost.Tezka;
      #endregion

      if (ZmenaHracu) {
        //if (!hlavniOkno.IsBtnPauseOn) {
        //  hlavniOkno.PauseGameAction();
        //}
        ZmenaNastaveniHracu();
      }
      else {
        hlavniOkno.historie = false;
        //hlavniOkno.PauseGameAction();
        //hlavniOkno.IsPaused = false;
        // zpristupneni Nastaveni hracu v hlavni nabidce
        hlavniOkno.nastaveníHráčůToolStripMenuItem.Enabled = true;
        StartNovaHra();
      }

      // nastavi typy hracu v info liste
      hlavniOkno.NastavStatusHracu();
    }

    private void btnCancel_Click(object sender, EventArgs e) {
      //if (!hlavniOkno.IsBtnPauseOn)
      //  hlavniOkno.PauseGameAction();
    }

    private void rbHrac1Pocitac_CheckedChanged(object sender, EventArgs e) {
      if (rbHrac1Pocitac.Checked) {
        panHrac1.Enabled = true;
      }
      else {
        panHrac1.Enabled = false;
      }
    }

    private void rbHrac2Pocitac_CheckedChanged(object sender, EventArgs e) {
      if (rbHrac2Pocitac.Checked) {
        panHrac2.Enabled = true;
      }
      else {
        panHrac2.Enabled = false;
      }
    }

    private void NovaHra_Load(object sender, EventArgs e) {
      // typ hrace 1
      if (hlavniOkno.typHrace1 == TypHrace.Clovek)
        rbHrac1Clovek.Checked = true;
      else
        rbHrac1Pocitac.Checked = true;
      // typ hrace 2
      if (hlavniOkno.typHrace2 == TypHrace.Clovek)
        rbHrac2Clovek.Checked = true;
      else
        rbHrac2Pocitac.Checked = true;

      // obtiznost hrace 1
      switch (hlavniOkno.obtiznostH1) {
        case Obtiznost.Lehka:
          rbHrac1Lehka.Checked = true;
          break;
        case Obtiznost.Stredni:
          rbHrac1Stredni.Checked = true;
          break;
        case Obtiznost.Tezka:
          rbHrac1Tezka.Checked = true;
          break;
      }
      // obtiznost hrace 2
      switch (hlavniOkno.obtiznostH2) {
        case Obtiznost.Lehka:
          rbHrac2Lehka.Checked = true;
          break;
        case Obtiznost.Stredni:
          rbHrac2Stredni.Checked = true;
          break;
        case Obtiznost.Tezka:
          rbHrac2Tezka.Checked = true;
          break;
      }
    }

  }
}
