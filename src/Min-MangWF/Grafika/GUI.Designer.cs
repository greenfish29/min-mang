﻿namespace Min_Mang {
  partial class GUI {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GUI));
      this.menuStrip1 = new System.Windows.Forms.MenuStrip();
      this.souborToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.nováHraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.otevřítHruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.ulozitHruToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
      this.konecToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.nastaveníToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.nastaveníHráčůToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.nastaveníVzhleduToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
      this.undoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.redoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
      this.nejlepsiTahToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.nápovědaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.napovedaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
      this.oProgramuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
      this.Pad = new System.Windows.Forms.PictureBox();
      this.lbxHistorie = new System.Windows.Forms.ListBox();
      this.btnBackward = new System.Windows.Forms.Button();
      this.btnForward = new System.Windows.Forms.Button();
      this.btnPlay = new System.Windows.Forms.Button();
      this.statusStrip1 = new System.Windows.Forms.StatusStrip();
      this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.typyHracuLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.pocetFigurekLabel = new System.Windows.Forms.ToolStripStatusLabel();
      this.btnStart = new System.Windows.Forms.Button();
      this.btnEnd = new System.Windows.Forms.Button();
      this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
      this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
      this.menuStrip1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.Pad)).BeginInit();
      this.statusStrip1.SuspendLayout();
      this.SuspendLayout();
      // 
      // menuStrip1
      // 
      this.menuStrip1.BackColor = System.Drawing.SystemColors.ButtonFace;
      this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.souborToolStripMenuItem,
            this.nastaveníToolStripMenuItem,
            this.nápovědaToolStripMenuItem});
      resources.ApplyResources(this.menuStrip1, "menuStrip1");
      this.menuStrip1.Name = "menuStrip1";
      // 
      // souborToolStripMenuItem
      // 
      this.souborToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nováHraToolStripMenuItem,
            this.otevřítHruToolStripMenuItem,
            this.ulozitHruToolStripMenuItem,
            this.toolStripSeparator2,
            this.konecToolStripMenuItem});
      this.souborToolStripMenuItem.Name = "souborToolStripMenuItem";
      resources.ApplyResources(this.souborToolStripMenuItem, "souborToolStripMenuItem");
      // 
      // nováHraToolStripMenuItem
      // 
      this.nováHraToolStripMenuItem.Name = "nováHraToolStripMenuItem";
      resources.ApplyResources(this.nováHraToolStripMenuItem, "nováHraToolStripMenuItem");
      this.nováHraToolStripMenuItem.Click += new System.EventHandler(this.nováHraToolStripMenuItem_Click);
      // 
      // otevřítHruToolStripMenuItem
      // 
      this.otevřítHruToolStripMenuItem.Name = "otevřítHruToolStripMenuItem";
      resources.ApplyResources(this.otevřítHruToolStripMenuItem, "otevřítHruToolStripMenuItem");
      this.otevřítHruToolStripMenuItem.Click += new System.EventHandler(this.otevřítHruToolStripMenuItem_Click);
      // 
      // ulozitHruToolStripMenuItem
      // 
      this.ulozitHruToolStripMenuItem.Name = "ulozitHruToolStripMenuItem";
      resources.ApplyResources(this.ulozitHruToolStripMenuItem, "ulozitHruToolStripMenuItem");
      this.ulozitHruToolStripMenuItem.Click += new System.EventHandler(this.ulozitHruToolStripMenuItem_Click);
      // 
      // toolStripSeparator2
      // 
      this.toolStripSeparator2.Name = "toolStripSeparator2";
      resources.ApplyResources(this.toolStripSeparator2, "toolStripSeparator2");
      // 
      // konecToolStripMenuItem
      // 
      this.konecToolStripMenuItem.Name = "konecToolStripMenuItem";
      resources.ApplyResources(this.konecToolStripMenuItem, "konecToolStripMenuItem");
      this.konecToolStripMenuItem.Click += new System.EventHandler(this.konecToolStripMenuItem_Click);
      // 
      // nastaveníToolStripMenuItem
      // 
      this.nastaveníToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nastaveníHráčůToolStripMenuItem,
            this.nastaveníVzhleduToolStripMenuItem,
            this.toolStripSeparator3,
            this.undoToolStripMenuItem,
            this.redoToolStripMenuItem,
            this.toolStripSeparator4,
            this.nejlepsiTahToolStripMenuItem});
      this.nastaveníToolStripMenuItem.Name = "nastaveníToolStripMenuItem";
      resources.ApplyResources(this.nastaveníToolStripMenuItem, "nastaveníToolStripMenuItem");
      // 
      // nastaveníHráčůToolStripMenuItem
      // 
      resources.ApplyResources(this.nastaveníHráčůToolStripMenuItem, "nastaveníHráčůToolStripMenuItem");
      this.nastaveníHráčůToolStripMenuItem.Name = "nastaveníHráčůToolStripMenuItem";
      this.nastaveníHráčůToolStripMenuItem.Click += new System.EventHandler(this.nastaveníHráčůToolStripMenuItem_Click);
      // 
      // nastaveníVzhleduToolStripMenuItem
      // 
      this.nastaveníVzhleduToolStripMenuItem.Name = "nastaveníVzhleduToolStripMenuItem";
      resources.ApplyResources(this.nastaveníVzhleduToolStripMenuItem, "nastaveníVzhleduToolStripMenuItem");
      this.nastaveníVzhleduToolStripMenuItem.Click += new System.EventHandler(this.nastaveníVzhleduToolStripMenuItem_Click);
      // 
      // toolStripSeparator3
      // 
      this.toolStripSeparator3.Name = "toolStripSeparator3";
      resources.ApplyResources(this.toolStripSeparator3, "toolStripSeparator3");
      // 
      // undoToolStripMenuItem
      // 
      this.undoToolStripMenuItem.Name = "undoToolStripMenuItem";
      resources.ApplyResources(this.undoToolStripMenuItem, "undoToolStripMenuItem");
      this.undoToolStripMenuItem.Click += new System.EventHandler(this.undoToolStripMenuItem_Click);
      // 
      // redoToolStripMenuItem
      // 
      this.redoToolStripMenuItem.Name = "redoToolStripMenuItem";
      resources.ApplyResources(this.redoToolStripMenuItem, "redoToolStripMenuItem");
      this.redoToolStripMenuItem.Click += new System.EventHandler(this.redoToolStripMenuItem_Click);
      // 
      // toolStripSeparator4
      // 
      this.toolStripSeparator4.Name = "toolStripSeparator4";
      resources.ApplyResources(this.toolStripSeparator4, "toolStripSeparator4");
      // 
      // nejlepsiTahToolStripMenuItem
      // 
      this.nejlepsiTahToolStripMenuItem.Name = "nejlepsiTahToolStripMenuItem";
      resources.ApplyResources(this.nejlepsiTahToolStripMenuItem, "nejlepsiTahToolStripMenuItem");
      this.nejlepsiTahToolStripMenuItem.Click += new System.EventHandler(this.nejlepsiTahToolStripMenuItem_Click);
      // 
      // nápovědaToolStripMenuItem
      // 
      this.nápovědaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.napovedaToolStripMenuItem,
            this.toolStripSeparator1,
            this.oProgramuToolStripMenuItem});
      this.nápovědaToolStripMenuItem.Name = "nápovědaToolStripMenuItem";
      resources.ApplyResources(this.nápovědaToolStripMenuItem, "nápovědaToolStripMenuItem");
      // 
      // napovedaToolStripMenuItem
      // 
      this.napovedaToolStripMenuItem.Name = "napovedaToolStripMenuItem";
      resources.ApplyResources(this.napovedaToolStripMenuItem, "napovedaToolStripMenuItem");
      this.napovedaToolStripMenuItem.Click += new System.EventHandler(this.napovedaToolStripMenuItem_Click);
      // 
      // toolStripSeparator1
      // 
      this.toolStripSeparator1.Name = "toolStripSeparator1";
      resources.ApplyResources(this.toolStripSeparator1, "toolStripSeparator1");
      // 
      // oProgramuToolStripMenuItem
      // 
      this.oProgramuToolStripMenuItem.Name = "oProgramuToolStripMenuItem";
      resources.ApplyResources(this.oProgramuToolStripMenuItem, "oProgramuToolStripMenuItem");
      this.oProgramuToolStripMenuItem.Click += new System.EventHandler(this.oProgramuToolStripMenuItem_Click);
      // 
      // Pad
      // 
      this.Pad.BackColor = System.Drawing.Color.Gray;
      this.Pad.ErrorImage = null;
      this.Pad.InitialImage = null;
      resources.ApplyResources(this.Pad, "Pad");
      this.Pad.Name = "Pad";
      this.Pad.TabStop = false;
      this.Pad.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Pad_MouseMove);
      this.Pad.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Pad_MouseDown);
      this.Pad.Paint += new System.Windows.Forms.PaintEventHandler(this.Pad_Paint);
      this.Pad.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Pad_MouseUp);
      // 
      // lbxHistorie
      // 
      resources.ApplyResources(this.lbxHistorie, "lbxHistorie");
      this.lbxHistorie.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
      this.lbxHistorie.FormattingEnabled = true;
      this.lbxHistorie.Name = "lbxHistorie";
      this.lbxHistorie.UseTabStops = false;
      this.lbxHistorie.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.lbxHistorie_DrawItem);
      this.lbxHistorie.SelectedIndexChanged += new System.EventHandler(this.lbxHistorie_SelectedIndexChanged);
      // 
      // btnBackward
      // 
      resources.ApplyResources(this.btnBackward, "btnBackward");
      this.btnBackward.Name = "btnBackward";
      this.btnBackward.UseVisualStyleBackColor = true;
      this.btnBackward.Click += new System.EventHandler(this.btnBackward_Click);
      // 
      // btnForward
      // 
      resources.ApplyResources(this.btnForward, "btnForward");
      this.btnForward.Name = "btnForward";
      this.btnForward.UseVisualStyleBackColor = true;
      this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
      // 
      // btnPlay
      // 
      resources.ApplyResources(this.btnPlay, "btnPlay");
      this.btnPlay.Name = "btnPlay";
      this.btnPlay.UseVisualStyleBackColor = true;
      this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
      // 
      // statusStrip1
      // 
      this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel,
            this.typyHracuLabel,
            this.pocetFigurekLabel});
      resources.ApplyResources(this.statusStrip1, "statusStrip1");
      this.statusStrip1.Name = "statusStrip1";
      this.statusStrip1.SizingGrip = false;
      // 
      // statusLabel
      // 
      this.statusLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.statusLabel.Name = "statusLabel";
      resources.ApplyResources(this.statusLabel, "statusLabel");
      this.statusLabel.Spring = true;
      // 
      // typyHracuLabel
      // 
      resources.ApplyResources(this.typyHracuLabel, "typyHracuLabel");
      this.typyHracuLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
      this.typyHracuLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.typyHracuLabel.Name = "typyHracuLabel";
      // 
      // pocetFigurekLabel
      // 
      resources.ApplyResources(this.pocetFigurekLabel, "pocetFigurekLabel");
      this.pocetFigurekLabel.BorderSides = System.Windows.Forms.ToolStripStatusLabelBorderSides.Left;
      this.pocetFigurekLabel.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
      this.pocetFigurekLabel.Name = "pocetFigurekLabel";
      // 
      // btnStart
      // 
      resources.ApplyResources(this.btnStart, "btnStart");
      this.btnStart.Name = "btnStart";
      this.btnStart.UseVisualStyleBackColor = true;
      this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
      // 
      // btnEnd
      // 
      resources.ApplyResources(this.btnEnd, "btnEnd");
      this.btnEnd.Name = "btnEnd";
      this.btnEnd.UseVisualStyleBackColor = true;
      this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
      // 
      // saveFileDialog
      // 
      resources.ApplyResources(this.saveFileDialog, "saveFileDialog");
      // 
      // openFileDialog
      // 
      resources.ApplyResources(this.openFileDialog, "openFileDialog");
      // 
      // GUI
      // 
      resources.ApplyResources(this, "$this");
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.Controls.Add(this.btnEnd);
      this.Controls.Add(this.btnStart);
      this.Controls.Add(this.statusStrip1);
      this.Controls.Add(this.btnPlay);
      this.Controls.Add(this.btnForward);
      this.Controls.Add(this.btnBackward);
      this.Controls.Add(this.lbxHistorie);
      this.Controls.Add(this.Pad);
      this.Controls.Add(this.menuStrip1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MainMenuStrip = this.menuStrip1;
      this.MaximizeBox = false;
      this.Name = "GUI";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.GUI_FormClosing);
      this.menuStrip1.ResumeLayout(false);
      this.menuStrip1.PerformLayout();
      ((System.ComponentModel.ISupportInitialize)(this.Pad)).EndInit();
      this.statusStrip1.ResumeLayout(false);
      this.statusStrip1.PerformLayout();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.MenuStrip menuStrip1;
    private System.Windows.Forms.ToolStripMenuItem souborToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem nastaveníToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem nápovědaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem napovedaToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem oProgramuToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem nováHraToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem otevřítHruToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem ulozitHruToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    private System.Windows.Forms.ToolStripMenuItem konecToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
    private System.Windows.Forms.PictureBox Pad;
    public System.Windows.Forms.ToolStripMenuItem nastaveníHráčůToolStripMenuItem;
    private System.Windows.Forms.ToolStripMenuItem nastaveníVzhleduToolStripMenuItem;
    private System.Windows.Forms.ListBox lbxHistorie;
    private System.Windows.Forms.Button btnBackward;
    private System.Windows.Forms.Button btnForward;
    private System.Windows.Forms.ToolStripMenuItem redoToolStripMenuItem;
    private System.Windows.Forms.Button btnPlay;
    private System.Windows.Forms.StatusStrip statusStrip1;
    private System.Windows.Forms.ToolStripStatusLabel statusLabel;
    private System.Windows.Forms.ToolStripMenuItem nejlepsiTahToolStripMenuItem;
    private System.Windows.Forms.Button btnStart;
    private System.Windows.Forms.Button btnEnd;
    private System.Windows.Forms.ToolStripStatusLabel typyHracuLabel;
    private System.Windows.Forms.ToolStripStatusLabel pocetFigurekLabel;
    private System.Windows.Forms.SaveFileDialog saveFileDialog;
    private System.Windows.Forms.OpenFileDialog openFileDialog;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
    private System.Windows.Forms.ToolStripMenuItem undoToolStripMenuItem;
    private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
  }
}

