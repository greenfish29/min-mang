﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Xml;
using System.Xml.Schema;


namespace Min_Mang
{

  partial class GUI : Form
  {
    #region Items
    private const int velikostPole = 50;
    private const int velikostKamene = 30;
    private const int velikostOkraju = 110;

    public SolidBrush brushKameny;
    public Color kameny1 = Color.White;
    public Color kameny2 = Color.Black;
    public SolidBrush brushPole;
    public Color pole1 = Color.BurlyWood;
    public Color pole2 = Color.SteelBlue;
    public Pen penObrysPole;
    public Color obrysPole = Color.Black;
    public Pen penObrysFigurky;
    public Color obrysKamenu1 = Color.Black;
    public Color obrysKamenu2 = Color.White;

    private Point[] poleSouradnicKamenu;
    public Tah[] poleMoznychTahu;
    private bool kresliMozneTahy = false;
    public bool kresliKameny = false;

    //public StavHraciDesky aktualniStavDesky = new StavHraciDesky();
    public StavHraciDesky aktualniStavDesky;

    //public MainWindowInterface posluchacUdalosti;
    public NastaveniVzhledu nastaveniVzhledu;
    public event ZmenaStavuHandler StartNovaHra;
    public event ZmenaStavuHandler ZmenaNastaveniHracu;
    public event ZmenaStavuHandler Undo;
    public event ZmenaStavuHandler Rendo;
    public event ZmenaStavuHandler ObnovHru;
    public event ZmenaStavuHandler Loading;
    //public event ZmenaStavuHandler ProvedeniTahuEvent;
    public bool povoleneTahnuti;
    public Tah provadenyTah;
    public HerniBarva komuPovolitTahy;
    public bool formClosed;
    public TypHrace typHrace1;
    public TypHrace typHrace2;
    public Obtiznost obtiznostH1;
    public Obtiznost obtiznostH2;
    public bool historie = false;
    public EventWaitHandle ewh;
    public bool IsBestMoveEnabled { get; set; }
    public bool IsGameRepeated { get; set; }
    public bool LoadWithError { get; set; }
    #endregion

    public GUI() {
      InitializeComponent();
      poleSouradnicKamenu = new Point[81];
      nastaveniVzhledu = new NastaveniVzhledu(this);

      komuPovolitTahy = HerniBarva.BILY;
      obtiznostH1 = Obtiznost.Lehka;
      obtiznostH2 = Obtiznost.Lehka;
      typHrace2 = TypHrace.Pocitac;
      formClosed = false;
      povoleneTahnuti = false;
      // nastaveni barev stetcu
      brushKameny = new SolidBrush(kameny1);
      brushPole = new SolidBrush(pole1);
      penObrysPole = new Pen(obrysPole, 2);
      penObrysFigurky = new Pen(obrysKamenu2, 2);
      // nastavi tlacitka a polozky menu na prochazeni historie na disabled
      btnBackward.Enabled = false;
      btnForward.Enabled = false;
      btnPlay.Enabled = false;
      btnStart.Enabled = false;
      btnEnd.Enabled = false;
      undoToolStripMenuItem.Enabled = false;
      redoToolStripMenuItem.Enabled = false;
      nejlepsiTahToolStripMenuItem.Enabled = false;
      ulozitHruToolStripMenuItem.Enabled = false;
      IsPaused = false;
      IsBtnReaction = false;
      IsGameRepeated = false;
      SetInfoText(InfoText.Uvitani);
    }

    private void konecToolStripMenuItem_Click(object sender, EventArgs e) {
      formClosed = true;
      Close();
    }


    /// <summary>
    /// Stará se o kreslení hrací plochy a kamenů
    /// </summary>
    private void Pad_Paint(object sender, PaintEventArgs e) {

      e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;

      for (int i = 0; i < 8; i++) {
        brushPole.Color = brushPole.Color == pole1 ? pole2 : pole1;
        penObrysPole.Color = obrysPole;
        for (int j = 0; j < 8; j++) {

          e.Graphics.FillRectangle(brushPole, (i - 1) * velikostPole + velikostOkraju, (j - 1) * velikostPole + (Pad.Height - (velikostOkraju + 6 * velikostPole)), velikostPole, velikostPole);
          e.Graphics.DrawRectangle(penObrysPole, (i - 1) * velikostPole + velikostOkraju, (j - 1) * velikostPole + (Pad.Height - (velikostOkraju + 6 * velikostPole)), velikostPole, velikostPole);
          brushPole.Color = brushPole.Color == pole1 ? pole2 : pole1;
        }
      }

      VykresliOsy(e);
      if (kresliKameny)
        VykresliKameny(e);
      if (kresliMozneTahy) {
        VykresliMozneTahy(e);
      }
    }

    // Štětec pro kreslení os
    private SolidBrush brushOsy = new SolidBrush(Color.Black);
    /// <summary>
    /// Vykresli souradne osy hraci desky
    /// </summary>
    private void VykresliOsy(PaintEventArgs e) {
      Font pismo = new Font("Arial", 12, FontStyle.Bold);
      char pismeno = 'A';

      for (int i = 0; i < 9; i++) {
        int bod = velikostPole * i + ((velikostOkraju / 2) - 3);
        e.Graphics.DrawString(pismeno.ToString(), pismo, brushOsy, bod, 470);
        pismeno++;
      }

      for (int i = 0; i < 9; i++) {
        int bod = velikostPole * i + (((Pad.Height - (velikostOkraju + 6 * velikostPole)) / 2) - 12);
        e.Graphics.DrawString((9 - i).ToString(), pismo, brushOsy, 15, bod);
      }
    }

    /// <summary>
    /// Vykreslí rozestavení kamenů podle aktuálního stavu hrací desky
    /// </summary>
    private void VykresliKameny(PaintEventArgs e) {
      for (int i = 0; i < 81; i++) {
        if (aktualniStavDesky.VratHodnotuPole(i) != HerniBarva.PRAZDNY) {
          brushKameny.Color = aktualniStavDesky.VratHodnotuPole(i) == HerniBarva.BILY ? kameny1 : kameny2;
          e.Graphics.FillEllipse(brushKameny, poleSouradnicKamenu[i].X, poleSouradnicKamenu[i].Y, velikostKamene, velikostKamene);
          penObrysFigurky.Color = aktualniStavDesky.VratHodnotuPole(i) == HerniBarva.CERNY ? obrysKamenu2 : obrysKamenu1;
          e.Graphics.DrawEllipse(penObrysFigurky, poleSouradnicKamenu[i].X, poleSouradnicKamenu[i].Y, velikostKamene, velikostKamene);
        }
      }
    }

    // Pera a štětce pro kreslení možných tahů
    private Pen penMozneTahy = new Pen(Color.Black, 1);
    private Brush brushMozneTahy = new SolidBrush(Color.FromArgb(100, Color.Green));
    /// <summary>
    /// Stará se o kreslení koleček možných tahů
    /// </summary>
    void VykresliMozneTahy(PaintEventArgs e) {
      foreach (Tah tah in poleMoznychTahu) {
        e.Graphics.DrawEllipse(penMozneTahy, (velikostOkraju - velikostPole + (tah.Kam % 9) * velikostPole) - (velikostKamene / 2), ((Pad.Height - (velikostOkraju + 6 * velikostPole)) - velikostPole + (tah.Kam / 9) * velikostPole) - (velikostKamene / 2), velikostKamene, velikostKamene);
        e.Graphics.FillEllipse(brushMozneTahy, (velikostOkraju - velikostPole + (tah.Kam % 9) * velikostPole) - (velikostKamene / 2), ((Pad.Height - (velikostOkraju + 6 * velikostPole)) - velikostPole + (tah.Kam / 9) * velikostPole) - (velikostKamene / 2), velikostKamene, velikostKamene);
      }
    }


    int rozdilX;
    int rozdilY;
    public int pohybovanyKamen = -1;

    /// <summary>
    /// Událost při stisku tlačítka myši
    /// </summary>
    private void Pad_MouseDown(object sender, MouseEventArgs e) {
      if (IsPaused && povoleneTahnuti)
        povoleneTahnuti = false;

      if (aktualniStavDesky != null && povoleneTahnuti) {
        for (int i = 0; i < poleSouradnicKamenu.Length; i++) {
          if (aktualniStavDesky.VratHodnotuPole(i) != komuPovolitTahy) {
            continue;
          }
          if (new Rectangle(poleSouradnicKamenu[i].X, poleSouradnicKamenu[i].Y, velikostKamene, velikostKamene).Contains(e.Location)) {
            rozdilX = e.Location.X - poleSouradnicKamenu[i].X;
            rozdilY = e.Location.Y - poleSouradnicKamenu[i].Y;
            pohybovanyKamen = i;

            if (aktualniStavDesky.SpocitejKameny(aktualniStavDesky.VratHodnotuPole(pohybovanyKamen)) == 1) {
              poleMoznychTahu = Rozhodci.DejMnozinuMoznychTahuPreskoky(aktualniStavDesky, pohybovanyKamen);
            }
            else {
              poleMoznychTahu = Rozhodci.DejMnozinuMoznychTahu(aktualniStavDesky, pohybovanyKamen);
            }

            kresliMozneTahy = true;
            break;
          }
        }
        Pad.Invalidate();
      }

      //Pad.Invalidate();
    }

    public int obalPrekresleni = 3;
    /// <summary>
    /// Událost při držení tlačítka a tažení myší
    /// </summary>
    private void Pad_MouseMove(object sender, MouseEventArgs e) {
      if (pohybovanyKamen != -1) {
        poleSouradnicKamenu[pohybovanyKamen] = new Point(e.Location.X - rozdilX, e.Location.Y - rozdilY);
        Rectangle oblastPrekresleni = new Rectangle(e.X - obalPrekresleni * velikostPole, e.Y - obalPrekresleni * velikostPole,
                                      2 * obalPrekresleni * velikostPole, 2 * obalPrekresleni * velikostPole);
        Pad.Invalidate(oblastPrekresleni);
      }
    }

    /// <summary>
    /// Událost při uvolnění tlačítka myši
    /// </summary>
    private void Pad_MouseUp(object sender, MouseEventArgs e) {
      if (pohybovanyKamen == -1)
        return;

      for (int i = 0; i < poleSouradnicKamenu.Length; i++) {
        if (new Rectangle(velikostOkraju / 2 + (i % 9) * velikostPole - (velikostKamene / 2) + 5,
                          (Pad.Height - (velikostOkraju + 6 * velikostPole)) / 2 + (i / 9) * velikostPole - (velikostKamene / 2) - 5,
                          velikostKamene, velikostKamene).Contains(e.Location)) {
          provadenyTah = new Tah(pohybovanyKamen, i);
          break;
        }
      }
      ewh.Set();
      pohybovanyKamen = -1;
      kresliMozneTahy = false;
    }

    public void aktualizujSouradniceFigurek(bool repaint) {
      if (aktualniStavDesky != null) {
        HerniBarva s;
        for (int i = 0; i < poleSouradnicKamenu.Length; i++) {
          s = aktualniStavDesky.VratHodnotuPole(i);
          int aktX, aktY;
          if (s == HerniBarva.PRAZDNY) {
            poleSouradnicKamenu[i] = Point.Empty;
          }
          else {
            aktX = velikostOkraju - velikostPole + (i % 9) * velikostPole;
            aktY = (Pad.Height - (velikostOkraju + 6 * velikostPole)) - velikostPole + (i / 9) * velikostPole;
            poleSouradnicKamenu[i] = new Point(aktX - (velikostKamene / 2), aktY - (velikostKamene / 2));
          }

        }
      }

      if (aktualniStavDesky != null) {

        if (historie) {
          int pocet = aktualniStavDesky.HistorieTahu.Count;
          ZobrazTahDoHistorie(aktualniStavDesky.HistorieTahu[pocet - 1], pocet);
          historie = false;

          if (this.InvokeRequired) {
            this.Invoke((Action)(() =>
              {
                if (btnPlay.Enabled == false)
                  btnPlay.Enabled = true;
              }));
          }
        }

        NastavStatusPoctuKamenu();
      }

      if (repaint) {
        Pad.Invalidate();
      }
    }

    private void nováHraToolStripMenuItem_Click(object sender, EventArgs e) {
      NovaHra novaHra = new NovaHra(this);
      novaHra.StartNovaHra += new ZmenaStavuHandler(InformujNovaHra);
      novaHra.Text = "Nová hra";
      novaHra.ZmenaHracu = false;

      if (!IsBtnPauseOn) {
        IsPaused = true;
      }

      novaHra.ShowDialog();

      if (novaHra.DialogResult == DialogResult.OK) {
        lbxHistorie.Items.Clear();
        btnPlay.Enabled = true;
        btnBackward.Enabled = false;
        btnForward.Enabled = false;
        btnStart.Enabled = false;
        btnEnd.Enabled = false;

        IsPaused = false;
        IsBtnPauseOn = false;
        IsGameRepeated = false;
        btnPlay.Text = "| |";
        if (!ulozitHruToolStripMenuItem.Enabled)
          ulozitHruToolStripMenuItem.Enabled = true;
      }

      if (novaHra.DialogResult == DialogResult.Cancel) {
        if (!IsBtnPauseOn) {
          IsPaused = false;
          if (ewh != null) {
            ewh.Set();
          }
        }

      }
    }

    private void nastaveníHráčůToolStripMenuItem_Click(object sender, EventArgs e) {
      NovaHra novaHra = new NovaHra(this);
      novaHra.ZmenaNastaveniHracu += new ZmenaStavuHandler(InformujZmenaHracu);
      novaHra.Text = "Nastavení hráčů";
      novaHra.ZmenaHracu = true;

      if (!IsBtnPauseOn) {
        IsPaused = true;
      }

      novaHra.ShowDialog();

      if (novaHra.DialogResult == DialogResult.OK) {
        if (!IsBtnPauseOn) {
          IsPaused = false;
          ewh.Set();
        }
      }

      if (novaHra.DialogResult == DialogResult.Cancel) {
        if (!IsBtnPauseOn) {
          IsPaused = false;
          ewh.Set();
        }
      }

    }

    public void InformujNovaHra() {
      StartNovaHra();
    }

    public void InformujZmenaHracu() {
      ZmenaNastaveniHracu();
    }

    public void NastavKomuPovolitTahy(HerniBarva barva) {
      komuPovolitTahy = barva;
    }

    public void NastavPovoleneTahnuti(bool value) {
      povoleneTahnuti = value;
    }

    public void NastavEWH(EventWaitHandle value) {
      ewh = value;
    }

    private void GUI_FormClosing(object sender, FormClosingEventArgs e) {
      formClosed = true;
      if (ewh != null) {
        IsPaused = false;
        ewh.Set();
      }
    }

    private void nastaveníVzhleduToolStripMenuItem_Click(object sender, EventArgs e) {
      nastaveniVzhledu.ShowDialog();
    }

    public bool IsClickReaction { get; set; }
    private void lbxHistorie_SelectedIndexChanged(object sender, EventArgs e) {
      if (!IsBtnReaction) {

        if (lbxHistorie.SelectedIndex < AktualniPozice - 1) {
          IsClickReaction = true;
          for (int i = AktualniPozice - 1; i > lbxHistorie.SelectedIndex; i--) {
            graphicsUndo();
          }
        }

        if (lbxHistorie.SelectedIndex > AktualniPozice - 1) {
          IsClickReaction = true;
          for (int i = AktualniPozice - 1; i < lbxHistorie.SelectedIndex; i++) {
            graphicsRedo();
          }
        }

      }

      HistoryButtons();
      IsBtnReaction = false;
    }


    public int AktualniPozice { get; set; }

    /// <summary>
    /// Zobrazí tah do historie v textové podobě
    /// </summary>
    /// <param name="t">Tah, který se má zobrazit</param>
    /// <param name="pocet">Pořadí tahu, který se má zobrazit</param>
    public void ZobrazTahDoHistorie(Tah t, int pocet) {
      string text = pocet.ToString() + ".     " + t.ToString();
      if (t.VyhozeneIndexy.Length > 0) {
        text += "  x  (";
        foreach (int i in t.VyhozeneIndexy) {
          if (i != t.Odkud)
            text += Tah.prevedNaSouradnice(i) + " ";
        }
        text += ")";
      }

      if (this.lbxHistorie.InvokeRequired && !lbxHistorie.IsDisposed) {
        this.Invoke((Action)(() =>
          {
            lbxHistorie.Items.Add(text);
            lbxHistorie.SelectedIndex = AktualniPozice - 1;
          }));
      }
      else {
        lbxHistorie.Items.Add(text);
        lbxHistorie.SelectedIndex = AktualniPozice - 1;
      }
    }


    /// <summary>
    /// Odstraní tahy z historie, pokud je potřeba
    /// </summary>
    public bool SmazatHistorii { get; set; }
    public void SmazPolozkyHistorie() {
      if (this.InvokeRequired) {
        //this.Invoke((Action)(() =>
        //  {
        //    for (int i = lbxHistorie.Items.Count-1; i > AktualniPozice-1; i--) {
        //      this.lbxHistorie.Items.RemoveAt(i);
        //    }
        //  }));
      }
      else {
        for (int i = lbxHistorie.Items.Count - 1; i > AktualniPozice - 1; i--) {
          this.lbxHistorie.Items.RemoveAt(i);
        }
      }
    }


    //
    // Reakce na Undo/Redo po kliknutí na tlačítka.
    //
    #region Buttons Undo/Redo Actions
    private void btnBackward_Click(object sender, EventArgs e) {
      IsBtnReaction = true;
      IsClickReaction = false;
      graphicsUndo();
    }

    private void btnForward_Click(object sender, EventArgs e) {
      IsBtnReaction = true;
      IsClickReaction = false;
      graphicsRedo();
    }

    private void btnStart_Click(object sender, EventArgs e) {
      IsBtnReaction = false;
      lbxHistorie.SelectedIndex = -1;
    }

    private void btnEnd_Click(object sender, EventArgs e) {
      IsBtnReaction = false;
      lbxHistorie.SelectedIndex = lbxHistorie.Items.Count - 1;
    }
    #endregion

    //
    // Reakce na Undo/Redo po kliknutí v hlavní nabídce
    //
    #region Tool Strip Items Undo/Redo Actions
    private void undoToolStripMenuItem_Click(object sender, EventArgs e) {
      btnBackward_Click(sender, e);
    }

    private void redoToolStripMenuItem_Click(object sender, EventArgs e) {
      btnForward_Click(sender, e);
    }
    #endregion

    //
    // Funkce provádějící Undo/Redo v grafice a volají manažera
    //
    #region Graphics Undo/Redo
    public bool IsBtnReaction { get; set; }
    private void graphicsUndo() {
      if (AktualniPozice > 0) {
        Undo();
        provadenyTah = null;
        if (!IsBtnPauseOn) {
          PauseGameAction();
          IsBtnPauseOn = true;
        }
        if (!IsClickReaction)
          lbxHistorie.SelectedIndex = AktualniPozice - 1;

        SetInfoText(InfoText.Pozastaveno);
      }
    }

    private void graphicsRedo() {
      if (AktualniPozice < lbxHistorie.Items.Count) {
        Rendo();
        provadenyTah = null;
        if (!IsBtnPauseOn) {
          PauseGameAction();
          IsBtnPauseOn = true;
        }
        //ewh.Set();
        if (!IsClickReaction)
          lbxHistorie.SelectedIndex = AktualniPozice - 1;
      }

      SetInfoText(InfoText.Pozastaveno);
    }
    #endregion


    /// <summary>
    /// Stará se o viditelnost tlačítek a menu položek na procházení historie
    /// </summary>
    private void HistoryButtons() {
      if (lbxHistorie.Items.Count == 0) {
        btnForward.Enabled = false;
        btnBackward.Enabled = false;
        btnStart.Enabled = false;
        btnEnd.Enabled = false;
        undoToolStripMenuItem.Enabled = false;
        redoToolStripMenuItem.Enabled = false;
      }
      else {
        if (lbxHistorie.Items.Count != AktualniPozice) {
          btnForward.Enabled = true;
          redoToolStripMenuItem.Enabled = true;
          btnEnd.Enabled = true;
        }
        else {
          btnForward.Enabled = false;
          redoToolStripMenuItem.Enabled = false;
          btnEnd.Enabled = false;
        }

        if (AktualniPozice == 0) {
          btnBackward.Enabled = false;
          undoToolStripMenuItem.Enabled = false;
          btnStart.Enabled = false;
        }
        else {
          btnBackward.Enabled = true;
          undoToolStripMenuItem.Enabled = true;
          btnStart.Enabled = true;
        }
      }
    }


    /// <summary>
    /// Stará se o pozastavování hry a samotné tlačítko
    /// </summary>
    public bool IsPaused { get; set; }
    public bool IsBtnPauseOn { get; set; }
    private void btnPlay_Click(object sender, EventArgs e) {
      if (IsBtnPauseOn) {
        IsBtnPauseOn = false;
      }
      else {
        IsBtnPauseOn = true;
        SetInfoText(InfoText.Pozastaveno);
      }

      PauseGameAction();
    }


    public void PauseGameAction() {
      if (IsPaused) {
        // pokud uzivatel chce spustit hru ze starsi pozice, zobrazi potvrzeni a nasledne vymaze historii
        if (SmazatHistorii) {
          if ((MessageBox.Show("Spuštěním hry z této pozice ztratíte nenávratně veškeré tahy po ní následující. Opravdu chcete pokračovat?",
            "Min-Mang", MessageBoxButtons.YesNo, MessageBoxIcon.Warning)) == DialogResult.Yes) {
            SmazPolozkyHistorie();
            SmazatHistorii = false;
            HistoryButtons();
            if (IsGameRepeated) {
              ObnovHru();
            }
          }
          else {
            IsBtnPauseOn = true;
            SetInfoText(InfoText.Pozastaveno);
            return;
          }
        }
        // nastaveni tlacitka play
        btnPlay.Text = "| |";
        IsPaused = false;
        // spusteni herniho vlakna
        if (ewh != null)
          ewh.Set();
      }
      else {
        // nastaveni tlacitka play
        btnPlay.Text = "►";
        IsPaused = true;
        nejlepsiTahToolStripMenuItem.Enabled = false;
        // zakazani tahnuti
        //povoleneTahnuti = false;
        // spusteni herniho vlakna
        //if(ewh != null)
        //  ewh.Set();
      }
    }


    /// <summary>
    /// Nápověda nejlepšího tahu, zobrazí dialog s nejlepším tahem
    /// </summary>
    public event ZmenaStavuHandler BestMove;
    private void nejlepsiTahToolStripMenuItem_Click(object sender, EventArgs e) {
      BestMove();
      NejlepsiTah msgBox = new NejlepsiTah("Nejlepší tah: " + provadenyTah.ToString());
      msgBox.ShowDialog();

      if (msgBox.DialogResult == DialogResult.Yes) {
        ewh.Set();
      }
    }


    /// <summary>
    /// Nastaví dostupnost položky menu nápovědy nejlepšího tahu
    /// </summary>
    public void NastavBestMoveEnabled(bool value) {
      if (this.InvokeRequired) {
        this.Invoke((Action)(() =>
          {
            this.nejlepsiTahToolStripMenuItem.Enabled = value;
          }));
      }
    }


    /// <summary>
    /// Vykresluje kazdy radek ListBoxu jinou barvou
    /// </summary>
    private void lbxHistorie_DrawItem(object sender, DrawItemEventArgs e) {
      SolidBrush hrac1 = new SolidBrush(Color.Black);
      SolidBrush hrac2 = new SolidBrush(Color.DarkRed);
      e.DrawBackground();

      if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
        e.Graphics.FillRectangle(Brushes.SkyBlue, e.Bounds);

      if (e.Index >= 0) {
        if (e.Index % 2 == 0)
          e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(), e.Font, hrac1, e.Bounds, StringFormat.GenericDefault);
        else
          e.Graphics.DrawString(((ListBox)sender).Items[e.Index].ToString(), e.Font, hrac2, e.Bounds, StringFormat.GenericDefault);
      }
      e.DrawFocusRectangle();
    }


    /// <summary>
    /// Nastavi info o typech hracu v info liste
    /// </summary>
    public void NastavStatusHracu() {
      string text;

      if (typHrace1 == TypHrace.Pocitac && typHrace2 == TypHrace.Pocitac)
        text = String.Format("{0}({1}) x {2}({3})", typHrace1, obtiznostH1, typHrace2, obtiznostH2);
      else if (typHrace1 == TypHrace.Pocitac)
        text = String.Format("{0}({1}) x {2}", typHrace1, obtiznostH1, typHrace2);
      else if (typHrace2 == TypHrace.Pocitac)
        text = String.Format("{0} x {1}({2})", typHrace1, typHrace2, obtiznostH2);
      else
        text = String.Format("{0} x {1}", typHrace1, typHrace2);

      typyHracuLabel.Text = text;
    }


    /// <summary>
    /// Nastavi info o poctu figurek v info liste
    /// </summary>
    public void NastavStatusPoctuKamenu() {
      int pocetBilych = aktualniStavDesky.SpocitejKameny(HerniBarva.BILY);
      int pocetCernych = aktualniStavDesky.SpocitejKameny(HerniBarva.CERNY);
      pocetFigurekLabel.Text = String.Format("{0} x {1}", pocetBilych, pocetCernych);
    }

    public void SetInfoText(InfoText infoText) {
      switch (infoText) {
        case InfoText.Uvitani:
          statusLabel.Text = "Pro spuštění nové hry zvolte Soubor -> Nová hra";
          break;
        case InfoText.Hraje1:
          statusLabel.Text = "Hráč 1 je na tahu";
          break;
        case InfoText.Hraje2:
          statusLabel.Text = "Hráč 2 je na tahu";
          break;
        case InfoText.Vyhral1:
          statusLabel.Text = "Hra skončila vítězstvím Hráče 1";
          break;
        case InfoText.Vyhral2:
          statusLabel.Text = "Hra skončila vítězstvím Hráče 2";
          break;
        case InfoText.Pozastaveno:
          statusLabel.Text = "Hra je pozastavena";
          break;
        case InfoText.Spusteno:
          statusLabel.Text = "Hra je spuštěna";
          break;
        default:
          statusLabel.Text = "";
          break;
      }
    }

    #region Ukladani a nacitani hry
    /// <summary>
    /// Vyvola dialog ulozeni hry
    /// </summary>
    private void ulozitHruToolStripMenuItem_Click(object sender, EventArgs e) {
      // nastaveni save file dialogu
      saveFileDialog.InitialDirectory = Path.GetFullPath(Directory.GetCurrentDirectory() + "/saves");
      saveFileDialog.DefaultExt = "xml";
      saveFileDialog.Filter = "XML soubor|*.xml";
      if(!IsBtnPauseOn)
        btnPlay_Click(btnPlay, e);

      if (saveFileDialog.ShowDialog() == DialogResult.OK) {
        try {
          SaveGame(saveFileDialog.FileName);
        }
        catch (Exception ex) {
          MessageBox.Show(ex.Message, "Chyba načítání hry", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
        }
      }
    }

    /// <summary>
    /// Provede vytvoreni samtneho xml souboru
    /// </summary>
    private void SaveGame(string fileName) {

      using (XmlTextWriter xml = new XmlTextWriter(fileName, System.Text.Encoding.UTF8)) {

        xml.Formatting = Formatting.Indented;
        xml.WriteStartDocument();
        xml.WriteStartElement("Hra");
        xml.WriteAttributeString("stav", IsGameRepeated.ToString());
        xml.WriteAttributeString("xmlns", "x-schema:schema.xdr");   

        // hraci
        xml.WriteStartElement("Hrac1");
        xml.WriteAttributeString("typ", typHrace1.ToString());
        xml.WriteAttributeString("obtiznost", obtiznostH1.ToString());
        xml.WriteEndElement();
        xml.WriteStartElement("Hrac2");
        xml.WriteAttributeString("typ", typHrace2.ToString());
        xml.WriteAttributeString("obtiznost", obtiznostH2.ToString());
        xml.WriteEndElement();

        foreach (Tah t in aktualniStavDesky.HistorieTahu) {
          xml.WriteStartElement("Tah");
          xml.WriteAttributeString("odkud", t.Odkud.ToString());
          xml.WriteAttributeString("kam", t.Kam.ToString());
          xml.WriteEndElement();
        }

        xml.WriteEndElement();

        xml.WriteEndDocument();
        xml.Close();

      }
    }

    /// <summary>
    /// Vyvola dialog nacteni ulozene hry
    /// </summary>
    private void otevřítHruToolStripMenuItem_Click(object sender, EventArgs e) {
      openFileDialog.InitialDirectory = Path.GetFullPath(Directory.GetCurrentDirectory() + "/saves");
      openFileDialog.Filter = "XML soubor|*.xml|Všechny soubory|*.*";
      openFileDialog.DefaultExt = "xml";

      if (!IsBtnPauseOn)
        btnPlay_Click(btnPlay, e);

      if (openFileDialog.ShowDialog() == DialogResult.OK) {
        StavHraciDesky zaloha;
        bool nastaveniHracuEnabled = nastaveníHráčůToolStripMenuItem.Enabled;
        bool btnPlayEnabled = btnPlay.Enabled;
        bool zalohaSmazatHistorii = SmazatHistorii;
        bool ulozitMenuItemEnabled = ulozitHruToolStripMenuItem.Enabled;
        int zalohaSelectedIndex = lbxHistorie.SelectedIndex;

        if (aktualniStavDesky != null) {
          zaloha = new StavHraciDesky(aktualniStavDesky);
        }
        else {
          zaloha = new StavHraciDesky();
          kresliKameny = true;
          nastaveníHráčůToolStripMenuItem.Enabled = true;
        }

        int zalohaAktualniPozice = AktualniPozice;
        HerniBarva zalohaKomuPovolitTahy = komuPovolitTahy;
        NastavKomuPovolitTahy(HerniBarva.BILY);
        btnPlay.Enabled = true;
        lbxHistorie.Items.Clear();
        SmazatHistorii = false;
        nejlepsiTahToolStripMenuItem.Enabled = false;

        try {
          LoadGame(openFileDialog.FileName);
          NastavStatusHracu();
          HistoryButtons();
          ulozitHruToolStripMenuItem.Enabled = true;
        }
        catch (Exception ex) {
          //kresliKameny = false;
          //aktualniStavDesky = new StavHraciDesky(zaloha);
          aktualniStavDesky = new StavHraciDesky();
          aktualniStavDesky.HistorieTahu = zaloha.HistorieTahu;
          foreach (Tah t in aktualniStavDesky.HistorieTahu) {
            aktualniStavDesky.ProvedTah(t);
            aktualniStavDesky.VyhodKameny(t.VyhozeneIndexy);
          }
          ulozitHruToolStripMenuItem.Enabled = ulozitMenuItemEnabled;
          AktualniPozice = 0;
          NastavKomuPovolitTahy(zalohaKomuPovolitTahy);
          btnPlay.Enabled = btnPlayEnabled;
          nastaveníHráčůToolStripMenuItem.Enabled = nastaveniHracuEnabled;
          SmazatHistorii = zalohaSmazatHistorii;
          // obnovi historii v listboxu
          lbxHistorie.Items.Clear();
          foreach (Tah t in aktualniStavDesky.HistorieTahu) {
            AktualniPozice++;
            ZobrazTahDoHistorie(t, AktualniPozice);
          }
          HistoryButtons();
          //LoadWithError = true;
          Loading();
          lbxHistorie.SelectedIndex = zalohaSelectedIndex;
          MessageBox.Show(ex.Message, "Chyba načítání hry", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
        }
      }

    }

    /// <summary>
    /// Provede vse potrebne pro nacteni hry, vcetne zachovani puvodni hry pri chybe
    /// </summary>
    private void LoadGame(string fileName) {

      aktualniStavDesky = new StavHraciDesky();
      AktualniPozice = 0;

      ParseXML(fileName);
      //HerniBarva kdoJeNaTahu = HerniBarva.BILY;

      foreach (Tah t in aktualniStavDesky.HistorieTahu) {
        if (komuPovolitTahy == aktualniStavDesky.VratHodnotuPole(t.Odkud)) {

          bool testJedineho = aktualniStavDesky.SpocitejKameny(komuPovolitTahy) == 1;
          bool testTahu;

          if (testJedineho) {
            testTahu = false;
            foreach (Tah tah in Rozhodci.DejMnozinuMoznychTahuPreskoky(aktualniStavDesky, t.Odkud)) {
              if (t.Kam == tah.Kam) {
                testTahu = true;
                break;
              }
            }
          }
          else {
            testTahu = Rozhodci.JeTahValidni(aktualniStavDesky, t);
          }


          if (testTahu) {

            aktualniStavDesky.ProvedTah(t);
            int[] vyhozeneIndexy = Rozhodci.DejIndexyVyhozeni(aktualniStavDesky, t);

            provadenyTah = t;
            provadenyTah.VyhozeneIndexy = vyhozeneIndexy;

            AktualniPozice += 1;

            aktualniStavDesky.VyhodKameny(vyhozeneIndexy);

            NastavKomuPovolitTahy((komuPovolitTahy == HerniBarva.BILY) ? HerniBarva.CERNY : HerniBarva.BILY);

            int pocet = lbxHistorie.Items.Count + 1;
            ZobrazTahDoHistorie(t, pocet);

          }
        }
        else {
          throw new Exception("Soubor s uloženou hrou obsahuje neplatný tah, byl zřejmě pozměněn, hru se proto nepodařilo načíst!");
        }
      }

      Loading();
    }

    /// <summary>
    /// Parsuje nacitany xml soubor
    /// </summary>
    private void ParseXML(string fileName) {

      using (XmlTextReader xml = new XmlTextReader(fileName)) {
        using (XmlValidatingReader val = new XmlValidatingReader(xml)) {

          // nastaveni typu overovani validity
          val.ValidationType = ValidationType.XDR;
          // pridani metody udalosti Validation 
          val.ValidationEventHandler += new ValidationEventHandler(ValidationEvent);

          while (val.Read()) {

            if (val.NodeType == XmlNodeType.Element) {

              switch (val.Name) {
                case "Hrac1":
                  string typ = val.GetAttribute("typ");
                  string obtiznost = val.GetAttribute("obtiznost");
                  // nastaveni typu hrace
                  if (typ.Equals("clovek", StringComparison.InvariantCultureIgnoreCase))
                    typHrace1 = TypHrace.Clovek;
                  else if (typ.Equals("pocitac", StringComparison.InvariantCultureIgnoreCase))
                    typHrace1 = TypHrace.Pocitac;
                  // nastaveni obtiznosti
                  if (obtiznost.Equals("lehka", StringComparison.InvariantCultureIgnoreCase))
                    obtiznostH1 = Obtiznost.Lehka;
                  else if (obtiznost.Equals("stredni", StringComparison.InvariantCultureIgnoreCase))
                    obtiznostH1 = Obtiznost.Stredni;
                  else if (obtiznost.Equals("tezka", StringComparison.InvariantCultureIgnoreCase))
                    obtiznostH1 = Obtiznost.Tezka;
                  break;
                case "Hrac2":
                  typ = val.GetAttribute("typ");
                  obtiznost = val.GetAttribute("obtiznost");
                  // nastaveni typu hracu
                  if (typ.Equals("clovek", StringComparison.InvariantCultureIgnoreCase))
                    typHrace2 = TypHrace.Clovek;
                  else if (typ.Equals("pocitac", StringComparison.InvariantCultureIgnoreCase))
                    typHrace2 = TypHrace.Pocitac;
                  // nastaveni obtiznosti
                  if (obtiznost.Equals("lehka", StringComparison.InvariantCultureIgnoreCase))
                    obtiznostH2 = Obtiznost.Lehka;
                  else if (obtiznost.Equals("stredni", StringComparison.InvariantCultureIgnoreCase))
                    obtiznostH2 = Obtiznost.Stredni;
                  else if (obtiznost.Equals("tezka", StringComparison.InvariantCultureIgnoreCase))
                    obtiznostH2 = Obtiznost.Tezka;
                  break;
                case "Tah":
                  int odkud = int.Parse(val.GetAttribute("odkud"));
                  int kam = int.Parse(val.GetAttribute("kam"));
                  Tah t = new Tah(odkud, kam);
                  aktualniStavDesky.PridejTahDoHistorie(t);
                  break;
                case "Hra":
                  string stav = val.GetAttribute("stav");
                  if (stav.Equals("false", StringComparison.InvariantCultureIgnoreCase))
                    IsGameRepeated = false;
                  else if (stav.Equals("true", StringComparison.InvariantCultureIgnoreCase))
                    IsGameRepeated = true;
                  break;
                default:
                  //throw new Exception("Soubor s uloženou hrou obsahuje neplatné elementy");
                  break;
              }
            }
          }
        }
      }
    }

    public void ValidationEvent(object sender, ValidationEventArgs args) {
      throw new Exception("Soubor s uloženou hrou nemá požadovaný formát.");
    }
    #endregion

    /// <summary>
    /// Zobrazi napovedu v prohlizeci (klasicky html soubor)
    /// </summary>
    private void oProgramuToolStripMenuItem_Click(object sender, EventArgs e) {
      AboutBox oProgramu = new AboutBox();
      oProgramu.ShowDialog();
      oProgramu.Dispose();
    }

    private void napovedaToolStripMenuItem_Click(object sender, EventArgs e) {
      // kontrola existence souboru s napovedou
      if (File.Exists(Path.GetFullPath(Directory.GetCurrentDirectory() + "/Help.htm"))) {
        System.Diagnostics.Process.Start(Path.GetFullPath(Directory.GetCurrentDirectory()+"/Help.htm"));
      }
      else {
        MessageBox.Show("Nebyl nalezen soubor s nápovědou.", "Soubor nenalezen", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }
  }
}