﻿namespace Min_Mang {
  partial class NovaHra {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.gbHrac1 = new System.Windows.Forms.GroupBox();
      this.panHrac1 = new System.Windows.Forms.Panel();
      this.rbHrac1Tezka = new System.Windows.Forms.RadioButton();
      this.lblObtiznostHrac1 = new System.Windows.Forms.Label();
      this.rbHrac1Stredni = new System.Windows.Forms.RadioButton();
      this.rbHrac1Lehka = new System.Windows.Forms.RadioButton();
      this.lblTypHrace1 = new System.Windows.Forms.Label();
      this.rbHrac1Pocitac = new System.Windows.Forms.RadioButton();
      this.rbHrac1Clovek = new System.Windows.Forms.RadioButton();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.gbHrac2 = new System.Windows.Forms.GroupBox();
      this.panHrac2 = new System.Windows.Forms.Panel();
      this.rbHrac2Tezka = new System.Windows.Forms.RadioButton();
      this.lblObtiznostHrac2 = new System.Windows.Forms.Label();
      this.rbHrac2Stredni = new System.Windows.Forms.RadioButton();
      this.rbHrac2Lehka = new System.Windows.Forms.RadioButton();
      this.lblTypHrace2 = new System.Windows.Forms.Label();
      this.rbHrac2Pocitac = new System.Windows.Forms.RadioButton();
      this.rbHrac2Clovek = new System.Windows.Forms.RadioButton();
      this.gbHrac1.SuspendLayout();
      this.panHrac1.SuspendLayout();
      this.gbHrac2.SuspendLayout();
      this.panHrac2.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbHrac1
      // 
      this.gbHrac1.Controls.Add(this.panHrac1);
      this.gbHrac1.Controls.Add(this.lblTypHrace1);
      this.gbHrac1.Controls.Add(this.rbHrac1Pocitac);
      this.gbHrac1.Controls.Add(this.rbHrac1Clovek);
      this.gbHrac1.Location = new System.Drawing.Point(15, 12);
      this.gbHrac1.Name = "gbHrac1";
      this.gbHrac1.Size = new System.Drawing.Size(232, 180);
      this.gbHrac1.TabIndex = 0;
      this.gbHrac1.TabStop = false;
      this.gbHrac1.Text = "Hráč 1";
      // 
      // panHrac1
      // 
      this.panHrac1.Controls.Add(this.rbHrac1Tezka);
      this.panHrac1.Controls.Add(this.lblObtiznostHrac1);
      this.panHrac1.Controls.Add(this.rbHrac1Stredni);
      this.panHrac1.Controls.Add(this.rbHrac1Lehka);
      this.panHrac1.Enabled = false;
      this.panHrac1.Location = new System.Drawing.Point(23, 86);
      this.panHrac1.Name = "panHrac1";
      this.panHrac1.Size = new System.Drawing.Size(189, 69);
      this.panHrac1.TabIndex = 6;
      // 
      // rbHrac1Tezka
      // 
      this.rbHrac1Tezka.AutoSize = true;
      this.rbHrac1Tezka.Location = new System.Drawing.Point(64, 50);
      this.rbHrac1Tezka.Name = "rbHrac1Tezka";
      this.rbHrac1Tezka.Size = new System.Drawing.Size(55, 17);
      this.rbHrac1Tezka.TabIndex = 2;
      this.rbHrac1Tezka.TabStop = true;
      this.rbHrac1Tezka.Text = "Těžká";
      this.rbHrac1Tezka.UseVisualStyleBackColor = true;
      // 
      // lblObtiznostHrac1
      // 
      this.lblObtiznostHrac1.AutoSize = true;
      this.lblObtiznostHrac1.Location = new System.Drawing.Point(-3, 5);
      this.lblObtiznostHrac1.Name = "lblObtiznostHrac1";
      this.lblObtiznostHrac1.Size = new System.Drawing.Size(56, 13);
      this.lblObtiznostHrac1.TabIndex = 5;
      this.lblObtiznostHrac1.Text = "Obtížnost:";
      // 
      // rbHrac1Stredni
      // 
      this.rbHrac1Stredni.AutoSize = true;
      this.rbHrac1Stredni.Location = new System.Drawing.Point(64, 27);
      this.rbHrac1Stredni.Name = "rbHrac1Stredni";
      this.rbHrac1Stredni.Size = new System.Drawing.Size(61, 17);
      this.rbHrac1Stredni.TabIndex = 1;
      this.rbHrac1Stredni.TabStop = true;
      this.rbHrac1Stredni.Text = "Střední";
      this.rbHrac1Stredni.UseVisualStyleBackColor = true;
      // 
      // rbHrac1Lehka
      // 
      this.rbHrac1Lehka.AutoSize = true;
      this.rbHrac1Lehka.Checked = true;
      this.rbHrac1Lehka.Location = new System.Drawing.Point(64, 4);
      this.rbHrac1Lehka.Name = "rbHrac1Lehka";
      this.rbHrac1Lehka.Size = new System.Drawing.Size(55, 17);
      this.rbHrac1Lehka.TabIndex = 0;
      this.rbHrac1Lehka.TabStop = true;
      this.rbHrac1Lehka.Text = "Lehká";
      this.rbHrac1Lehka.UseVisualStyleBackColor = true;
      // 
      // lblTypHrace1
      // 
      this.lblTypHrace1.AutoSize = true;
      this.lblTypHrace1.Location = new System.Drawing.Point(20, 35);
      this.lblTypHrace1.Name = "lblTypHrace1";
      this.lblTypHrace1.Size = new System.Drawing.Size(58, 13);
      this.lblTypHrace1.TabIndex = 3;
      this.lblTypHrace1.Text = "Typ hráče:";
      // 
      // rbHrac1Pocitac
      // 
      this.rbHrac1Pocitac.AutoSize = true;
      this.rbHrac1Pocitac.Location = new System.Drawing.Point(87, 56);
      this.rbHrac1Pocitac.Name = "rbHrac1Pocitac";
      this.rbHrac1Pocitac.Size = new System.Drawing.Size(63, 17);
      this.rbHrac1Pocitac.TabIndex = 1;
      this.rbHrac1Pocitac.Text = "Počítač";
      this.rbHrac1Pocitac.UseVisualStyleBackColor = true;
      this.rbHrac1Pocitac.CheckedChanged += new System.EventHandler(this.rbHrac1Pocitac_CheckedChanged);
      // 
      // rbHrac1Clovek
      // 
      this.rbHrac1Clovek.AutoSize = true;
      this.rbHrac1Clovek.Checked = true;
      this.rbHrac1Clovek.Location = new System.Drawing.Point(87, 35);
      this.rbHrac1Clovek.Name = "rbHrac1Clovek";
      this.rbHrac1Clovek.Size = new System.Drawing.Size(58, 17);
      this.rbHrac1Clovek.TabIndex = 0;
      this.rbHrac1Clovek.TabStop = true;
      this.rbHrac1Clovek.Text = "Člověk";
      this.rbHrac1Clovek.UseVisualStyleBackColor = true;
      // 
      // btnOk
      // 
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.Location = new System.Drawing.Point(331, 198);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(75, 23);
      this.btnOk.TabIndex = 1;
      this.btnOk.Text = "OK";
      this.btnOk.UseVisualStyleBackColor = true;
      this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(412, 198);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 8;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
      // 
      // gbHrac2
      // 
      this.gbHrac2.Controls.Add(this.panHrac2);
      this.gbHrac2.Controls.Add(this.lblTypHrace2);
      this.gbHrac2.Controls.Add(this.rbHrac2Pocitac);
      this.gbHrac2.Controls.Add(this.rbHrac2Clovek);
      this.gbHrac2.Location = new System.Drawing.Point(255, 12);
      this.gbHrac2.Name = "gbHrac2";
      this.gbHrac2.Size = new System.Drawing.Size(232, 180);
      this.gbHrac2.TabIndex = 7;
      this.gbHrac2.TabStop = false;
      this.gbHrac2.Text = "Hráč 2";
      // 
      // panHrac2
      // 
      this.panHrac2.Controls.Add(this.rbHrac2Tezka);
      this.panHrac2.Controls.Add(this.lblObtiznostHrac2);
      this.panHrac2.Controls.Add(this.rbHrac2Stredni);
      this.panHrac2.Controls.Add(this.rbHrac2Lehka);
      this.panHrac2.Enabled = false;
      this.panHrac2.Location = new System.Drawing.Point(23, 86);
      this.panHrac2.Name = "panHrac2";
      this.panHrac2.Size = new System.Drawing.Size(189, 69);
      this.panHrac2.TabIndex = 6;
      // 
      // rbHrac2Tezka
      // 
      this.rbHrac2Tezka.AutoSize = true;
      this.rbHrac2Tezka.Location = new System.Drawing.Point(64, 50);
      this.rbHrac2Tezka.Name = "rbHrac2Tezka";
      this.rbHrac2Tezka.Size = new System.Drawing.Size(55, 17);
      this.rbHrac2Tezka.TabIndex = 2;
      this.rbHrac2Tezka.TabStop = true;
      this.rbHrac2Tezka.Text = "Těžká";
      this.rbHrac2Tezka.UseVisualStyleBackColor = true;
      // 
      // lblObtiznostHrac2
      // 
      this.lblObtiznostHrac2.AutoSize = true;
      this.lblObtiznostHrac2.Location = new System.Drawing.Point(-3, 5);
      this.lblObtiznostHrac2.Name = "lblObtiznostHrac2";
      this.lblObtiznostHrac2.Size = new System.Drawing.Size(56, 13);
      this.lblObtiznostHrac2.TabIndex = 5;
      this.lblObtiznostHrac2.Text = "Obtížnost:";
      // 
      // rbHrac2Stredni
      // 
      this.rbHrac2Stredni.AutoSize = true;
      this.rbHrac2Stredni.Location = new System.Drawing.Point(64, 27);
      this.rbHrac2Stredni.Name = "rbHrac2Stredni";
      this.rbHrac2Stredni.Size = new System.Drawing.Size(61, 17);
      this.rbHrac2Stredni.TabIndex = 1;
      this.rbHrac2Stredni.TabStop = true;
      this.rbHrac2Stredni.Text = "Střední";
      this.rbHrac2Stredni.UseVisualStyleBackColor = true;
      // 
      // rbHrac2Lehka
      // 
      this.rbHrac2Lehka.AutoSize = true;
      this.rbHrac2Lehka.Checked = true;
      this.rbHrac2Lehka.Location = new System.Drawing.Point(64, 4);
      this.rbHrac2Lehka.Name = "rbHrac2Lehka";
      this.rbHrac2Lehka.Size = new System.Drawing.Size(55, 17);
      this.rbHrac2Lehka.TabIndex = 0;
      this.rbHrac2Lehka.TabStop = true;
      this.rbHrac2Lehka.Text = "Lehká";
      this.rbHrac2Lehka.UseVisualStyleBackColor = true;
      // 
      // lblTypHrace2
      // 
      this.lblTypHrace2.AutoSize = true;
      this.lblTypHrace2.Location = new System.Drawing.Point(20, 35);
      this.lblTypHrace2.Name = "lblTypHrace2";
      this.lblTypHrace2.Size = new System.Drawing.Size(58, 13);
      this.lblTypHrace2.TabIndex = 3;
      this.lblTypHrace2.Text = "Typ hráče:";
      // 
      // rbHrac2Pocitac
      // 
      this.rbHrac2Pocitac.AutoSize = true;
      this.rbHrac2Pocitac.Location = new System.Drawing.Point(87, 56);
      this.rbHrac2Pocitac.Name = "rbHrac2Pocitac";
      this.rbHrac2Pocitac.Size = new System.Drawing.Size(63, 17);
      this.rbHrac2Pocitac.TabIndex = 1;
      this.rbHrac2Pocitac.Text = "Počítač";
      this.rbHrac2Pocitac.UseVisualStyleBackColor = true;
      this.rbHrac2Pocitac.CheckedChanged += new System.EventHandler(this.rbHrac2Pocitac_CheckedChanged);
      // 
      // rbHrac2Clovek
      // 
      this.rbHrac2Clovek.AutoSize = true;
      this.rbHrac2Clovek.Checked = true;
      this.rbHrac2Clovek.Location = new System.Drawing.Point(87, 35);
      this.rbHrac2Clovek.Name = "rbHrac2Clovek";
      this.rbHrac2Clovek.Size = new System.Drawing.Size(58, 17);
      this.rbHrac2Clovek.TabIndex = 0;
      this.rbHrac2Clovek.TabStop = true;
      this.rbHrac2Clovek.Text = "Člověk";
      this.rbHrac2Clovek.UseVisualStyleBackColor = true;
      // 
      // NovaHra
      // 
      this.AcceptButton = this.btnOk;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(499, 229);
      this.Controls.Add(this.gbHrac2);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOk);
      this.Controls.Add(this.gbHrac1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "NovaHra";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Load += new System.EventHandler(this.NovaHra_Load);
      this.gbHrac1.ResumeLayout(false);
      this.gbHrac1.PerformLayout();
      this.panHrac1.ResumeLayout(false);
      this.panHrac1.PerformLayout();
      this.gbHrac2.ResumeLayout(false);
      this.gbHrac2.PerformLayout();
      this.panHrac2.ResumeLayout(false);
      this.panHrac2.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbHrac1;
    private System.Windows.Forms.RadioButton rbHrac1Pocitac;
    private System.Windows.Forms.RadioButton rbHrac1Clovek;
    private System.Windows.Forms.Label lblTypHrace1;
    private System.Windows.Forms.Label lblObtiznostHrac1;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Panel panHrac1;
    private System.Windows.Forms.RadioButton rbHrac1Tezka;
    private System.Windows.Forms.RadioButton rbHrac1Stredni;
    private System.Windows.Forms.RadioButton rbHrac1Lehka;
    private System.Windows.Forms.GroupBox gbHrac2;
    private System.Windows.Forms.Panel panHrac2;
    private System.Windows.Forms.RadioButton rbHrac2Tezka;
    private System.Windows.Forms.Label lblObtiznostHrac2;
    private System.Windows.Forms.RadioButton rbHrac2Stredni;
    private System.Windows.Forms.RadioButton rbHrac2Lehka;
    private System.Windows.Forms.Label lblTypHrace2;
    private System.Windows.Forms.RadioButton rbHrac2Pocitac;
    private System.Windows.Forms.RadioButton rbHrac2Clovek;
  }
}