﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Min_Mang
{
  public partial class NejlepsiTah : Form
  {
    public NejlepsiTah(string message) {
      InitializeComponent();
      this.lblText.Text = message;
      Bitmap b = Bitmap.FromHicon(System.Drawing.SystemIcons.Information.Handle);
      this.pictureBox1.Image = b;
    }

    private void btnOK_Click(object sender, EventArgs e) {
      Close();
    }

    private void btnDoMove_Click(object sender, EventArgs e) {
      Close();
    }

  }
}
