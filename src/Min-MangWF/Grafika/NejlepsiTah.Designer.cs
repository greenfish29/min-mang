﻿namespace Min_Mang
{
  partial class NejlepsiTah
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.btnOK = new System.Windows.Forms.Button();
      this.btnDoMove = new System.Windows.Forms.Button();
      this.lblText = new System.Windows.Forms.Label();
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // btnOK
      // 
      this.btnOK.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOK.Location = new System.Drawing.Point(38, 79);
      this.btnOK.Name = "btnOK";
      this.btnOK.Size = new System.Drawing.Size(75, 23);
      this.btnOK.TabIndex = 0;
      this.btnOK.Text = "OK";
      this.btnOK.UseVisualStyleBackColor = true;
      this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
      // 
      // btnDoMove
      // 
      this.btnDoMove.DialogResult = System.Windows.Forms.DialogResult.Yes;
      this.btnDoMove.Location = new System.Drawing.Point(119, 79);
      this.btnDoMove.Name = "btnDoMove";
      this.btnDoMove.Size = new System.Drawing.Size(75, 23);
      this.btnDoMove.TabIndex = 1;
      this.btnDoMove.Text = "Proveď tah";
      this.btnDoMove.UseVisualStyleBackColor = true;
      this.btnDoMove.Click += new System.EventHandler(this.btnDoMove_Click);
      // 
      // lblText
      // 
      this.lblText.AutoSize = true;
      this.lblText.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
      this.lblText.Location = new System.Drawing.Point(67, 36);
      this.lblText.Name = "lblText";
      this.lblText.Size = new System.Drawing.Size(0, 13);
      this.lblText.TabIndex = 2;
      this.lblText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // pictureBox1
      // 
      this.pictureBox1.ErrorImage = null;
      this.pictureBox1.InitialImage = null;
      this.pictureBox1.Location = new System.Drawing.Point(29, 26);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(56, 47);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
      this.pictureBox1.TabIndex = 3;
      this.pictureBox1.TabStop = false;
      // 
      // NejlepsiTah
      // 
      this.AcceptButton = this.btnOK;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
      this.ClientSize = new System.Drawing.Size(232, 114);
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.lblText);
      this.Controls.Add(this.btnDoMove);
      this.Controls.Add(this.btnOK);
      this.DoubleBuffered = true;
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "NejlepsiTah";
      this.ShowIcon = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Nejlepší tah";
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button btnOK;
    private System.Windows.Forms.Button btnDoMove;
    private System.Windows.Forms.Label lblText;
    private System.Windows.Forms.PictureBox pictureBox1;
  }
}