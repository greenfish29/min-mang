﻿using System.Windows.Forms;
using System.Drawing;

namespace Min_Mang
{
  partial class NastaveniVzhledu : Form
  {

    GUI hlavniOkno;

    public NastaveniVzhledu(GUI gui) {
      InitializeComponent();
      hlavniOkno = gui;
    }

    private void btnOk_Click(object sender, System.EventArgs e) {
      hlavniOkno.kameny1 = txtTelo1.BackColor;
      hlavniOkno.kameny2 = txtTelo2.BackColor;
      hlavniOkno.obrysKamenu1 = txtOkraj1.BackColor;
      hlavniOkno.obrysKamenu2 = txtOkraj2.BackColor;
      hlavniOkno.pole1 = txtHraciDeska1.BackColor;
      hlavniOkno.pole2 = txtHraciDeska2.BackColor;
      hlavniOkno.obrysPole = txtHraciDeskaObrys.BackColor;
      // Zabrání znovuzapsání tahu do historie
      hlavniOkno.historie = false;
      hlavniOkno.aktualizujSouradniceFigurek(true);
    }

    private void ZmenBarvu(TextBox t) {
      ColorDialog color = new ColorDialog();
      if (color.ShowDialog() == DialogResult.OK)
        t.BackColor = color.Color;
    }

    private void btnTelo1_Click(object sender, System.EventArgs e) {
      ZmenBarvu(txtTelo1);
    }

    private void btnTelo2_Click(object sender, System.EventArgs e) {
      ZmenBarvu(txtTelo2);
    }

    private void btnOkraj1_Click(object sender, System.EventArgs e) {
      ZmenBarvu(txtOkraj1);
    }

    private void btnOkraj2_Click(object sender, System.EventArgs e) {
      ZmenBarvu(txtOkraj2);
    }

    private void btnHraciDeska1_Click(object sender, System.EventArgs e) {
      ZmenBarvu(txtHraciDeska1);
    }

    private void btnHraciDeska2_Click(object sender, System.EventArgs e) {
      ZmenBarvu(txtHraciDeska2);
    }

    private void btnHraciDeskaObrys_Click(object sender, System.EventArgs e) {
      ZmenBarvu(txtHraciDeskaObrys);
    }

    private void btnDefault_Click(object sender, System.EventArgs e) {
      if ((MessageBox.Show("Opravdu chcete zrušit aktuální nastavení a použít nastavení původní?",
          "Min-Mang", MessageBoxButtons.YesNo, MessageBoxIcon.Question)) == DialogResult.Yes) {
        // nastavi barvy textboxu
        txtTelo1.BackColor = Color.White;
        txtTelo2.BackColor = Color.Black;
        txtOkraj1.BackColor = Color.Black;
        txtOkraj2.BackColor = Color.White;
        txtHraciDeska1.BackColor = Color.BurlyWood;
        txtHraciDeska2.BackColor = Color.SteelBlue;
        txtHraciDeskaObrys.BackColor = Color.Black;
      }
    }

    private void NastaveniVzhledu_Load(object sender, System.EventArgs e) {
      // Inicializace barev textboxu
      txtTelo1.BackColor = hlavniOkno.kameny1;
      txtTelo2.BackColor = hlavniOkno.kameny2;
      txtOkraj1.BackColor = hlavniOkno.obrysKamenu1;
      txtOkraj2.BackColor = hlavniOkno.obrysKamenu2;
      txtHraciDeska1.BackColor = hlavniOkno.pole1;
      txtHraciDeska2.BackColor = hlavniOkno.pole2;
      txtHraciDeskaObrys.BackColor = hlavniOkno.obrysPole;
    }
  }
}
