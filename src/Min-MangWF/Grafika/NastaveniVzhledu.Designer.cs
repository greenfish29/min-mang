﻿namespace Min_Mang
{
  partial class NastaveniVzhledu
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent() {
      this.gbHrac1 = new System.Windows.Forms.GroupBox();
      this.btnOkraj1 = new System.Windows.Forms.Button();
      this.txtOkraj1 = new System.Windows.Forms.TextBox();
      this.lblOkraj1 = new System.Windows.Forms.Label();
      this.btnTelo1 = new System.Windows.Forms.Button();
      this.txtTelo1 = new System.Windows.Forms.TextBox();
      this.lblTelo1 = new System.Windows.Forms.Label();
      this.gbHrac2 = new System.Windows.Forms.GroupBox();
      this.btnOkraj2 = new System.Windows.Forms.Button();
      this.txtOkraj2 = new System.Windows.Forms.TextBox();
      this.lblOkraj2 = new System.Windows.Forms.Label();
      this.btnTelo2 = new System.Windows.Forms.Button();
      this.txtTelo2 = new System.Windows.Forms.TextBox();
      this.lblTelo2 = new System.Windows.Forms.Label();
      this.gbHraciDeska = new System.Windows.Forms.GroupBox();
      this.btnHraciDeskaObrys = new System.Windows.Forms.Button();
      this.txtHraciDeskaObrys = new System.Windows.Forms.TextBox();
      this.lblHraciDeska2 = new System.Windows.Forms.Label();
      this.btnHraciDeska2 = new System.Windows.Forms.Button();
      this.txtHraciDeska2 = new System.Windows.Forms.TextBox();
      this.btnHraciDeska1 = new System.Windows.Forms.Button();
      this.txtHraciDeska1 = new System.Windows.Forms.TextBox();
      this.lblHraciDeska1 = new System.Windows.Forms.Label();
      this.btnOk = new System.Windows.Forms.Button();
      this.btnCancel = new System.Windows.Forms.Button();
      this.btnDefault = new System.Windows.Forms.Button();
      this.gbHrac1.SuspendLayout();
      this.gbHrac2.SuspendLayout();
      this.gbHraciDeska.SuspendLayout();
      this.SuspendLayout();
      // 
      // gbHrac1
      // 
      this.gbHrac1.Controls.Add(this.btnOkraj1);
      this.gbHrac1.Controls.Add(this.txtOkraj1);
      this.gbHrac1.Controls.Add(this.lblOkraj1);
      this.gbHrac1.Controls.Add(this.btnTelo1);
      this.gbHrac1.Controls.Add(this.txtTelo1);
      this.gbHrac1.Controls.Add(this.lblTelo1);
      this.gbHrac1.Location = new System.Drawing.Point(12, 12);
      this.gbHrac1.Name = "gbHrac1";
      this.gbHrac1.Size = new System.Drawing.Size(238, 90);
      this.gbHrac1.TabIndex = 0;
      this.gbHrac1.TabStop = false;
      this.gbHrac1.Text = "Hráč 1";
      // 
      // btnOkraj1
      // 
      this.btnOkraj1.Location = new System.Drawing.Point(168, 50);
      this.btnOkraj1.Name = "btnOkraj1";
      this.btnOkraj1.Size = new System.Drawing.Size(50, 23);
      this.btnOkraj1.TabIndex = 5;
      this.btnOkraj1.Text = "Změnit";
      this.btnOkraj1.UseVisualStyleBackColor = true;
      this.btnOkraj1.Click += new System.EventHandler(this.btnOkraj1_Click);
      // 
      // txtOkraj1
      // 
      this.txtOkraj1.Cursor = System.Windows.Forms.Cursors.Default;
      this.txtOkraj1.Enabled = false;
      this.txtOkraj1.Location = new System.Drawing.Point(118, 50);
      this.txtOkraj1.Multiline = true;
      this.txtOkraj1.Name = "txtOkraj1";
      this.txtOkraj1.ReadOnly = true;
      this.txtOkraj1.Size = new System.Drawing.Size(23, 23);
      this.txtOkraj1.TabIndex = 4;
      this.txtOkraj1.TabStop = false;
      // 
      // lblOkraj1
      // 
      this.lblOkraj1.AutoSize = true;
      this.lblOkraj1.Location = new System.Drawing.Point(18, 55);
      this.lblOkraj1.Name = "lblOkraj1";
      this.lblOkraj1.Size = new System.Drawing.Size(76, 13);
      this.lblOkraj1.TabIndex = 3;
      this.lblOkraj1.Text = "Okraj kamene:";
      // 
      // btnTelo1
      // 
      this.btnTelo1.Location = new System.Drawing.Point(168, 21);
      this.btnTelo1.Name = "btnTelo1";
      this.btnTelo1.Size = new System.Drawing.Size(50, 23);
      this.btnTelo1.TabIndex = 2;
      this.btnTelo1.Text = "Změnit";
      this.btnTelo1.UseVisualStyleBackColor = true;
      this.btnTelo1.Click += new System.EventHandler(this.btnTelo1_Click);
      // 
      // txtTelo1
      // 
      this.txtTelo1.Cursor = System.Windows.Forms.Cursors.Default;
      this.txtTelo1.Enabled = false;
      this.txtTelo1.Location = new System.Drawing.Point(118, 21);
      this.txtTelo1.Multiline = true;
      this.txtTelo1.Name = "txtTelo1";
      this.txtTelo1.ReadOnly = true;
      this.txtTelo1.Size = new System.Drawing.Size(23, 23);
      this.txtTelo1.TabIndex = 1;
      this.txtTelo1.TabStop = false;
      // 
      // lblTelo1
      // 
      this.lblTelo1.AutoSize = true;
      this.lblTelo1.Location = new System.Drawing.Point(18, 26);
      this.lblTelo1.Name = "lblTelo1";
      this.lblTelo1.Size = new System.Drawing.Size(72, 13);
      this.lblTelo1.TabIndex = 0;
      this.lblTelo1.Text = "Tělo kamene:";
      // 
      // gbHrac2
      // 
      this.gbHrac2.Controls.Add(this.btnOkraj2);
      this.gbHrac2.Controls.Add(this.txtOkraj2);
      this.gbHrac2.Controls.Add(this.lblOkraj2);
      this.gbHrac2.Controls.Add(this.btnTelo2);
      this.gbHrac2.Controls.Add(this.txtTelo2);
      this.gbHrac2.Controls.Add(this.lblTelo2);
      this.gbHrac2.Location = new System.Drawing.Point(258, 12);
      this.gbHrac2.Name = "gbHrac2";
      this.gbHrac2.Size = new System.Drawing.Size(238, 90);
      this.gbHrac2.TabIndex = 6;
      this.gbHrac2.TabStop = false;
      this.gbHrac2.Text = "Hráč 2";
      // 
      // btnOkraj2
      // 
      this.btnOkraj2.Location = new System.Drawing.Point(168, 50);
      this.btnOkraj2.Name = "btnOkraj2";
      this.btnOkraj2.Size = new System.Drawing.Size(50, 23);
      this.btnOkraj2.TabIndex = 5;
      this.btnOkraj2.Text = "Změnit";
      this.btnOkraj2.UseVisualStyleBackColor = true;
      this.btnOkraj2.Click += new System.EventHandler(this.btnOkraj2_Click);
      // 
      // txtOkraj2
      // 
      this.txtOkraj2.Cursor = System.Windows.Forms.Cursors.Default;
      this.txtOkraj2.Enabled = false;
      this.txtOkraj2.Location = new System.Drawing.Point(118, 50);
      this.txtOkraj2.Multiline = true;
      this.txtOkraj2.Name = "txtOkraj2";
      this.txtOkraj2.ReadOnly = true;
      this.txtOkraj2.Size = new System.Drawing.Size(23, 23);
      this.txtOkraj2.TabIndex = 4;
      this.txtOkraj2.TabStop = false;
      // 
      // lblOkraj2
      // 
      this.lblOkraj2.AutoSize = true;
      this.lblOkraj2.Location = new System.Drawing.Point(18, 55);
      this.lblOkraj2.Name = "lblOkraj2";
      this.lblOkraj2.Size = new System.Drawing.Size(76, 13);
      this.lblOkraj2.TabIndex = 3;
      this.lblOkraj2.Text = "Okraj kamene:";
      // 
      // btnTelo2
      // 
      this.btnTelo2.Location = new System.Drawing.Point(168, 21);
      this.btnTelo2.Name = "btnTelo2";
      this.btnTelo2.Size = new System.Drawing.Size(50, 23);
      this.btnTelo2.TabIndex = 2;
      this.btnTelo2.Text = "Změnit";
      this.btnTelo2.UseVisualStyleBackColor = true;
      this.btnTelo2.Click += new System.EventHandler(this.btnTelo2_Click);
      // 
      // txtTelo2
      // 
      this.txtTelo2.Cursor = System.Windows.Forms.Cursors.Default;
      this.txtTelo2.Enabled = false;
      this.txtTelo2.Location = new System.Drawing.Point(118, 21);
      this.txtTelo2.Multiline = true;
      this.txtTelo2.Name = "txtTelo2";
      this.txtTelo2.ReadOnly = true;
      this.txtTelo2.Size = new System.Drawing.Size(23, 23);
      this.txtTelo2.TabIndex = 1;
      this.txtTelo2.TabStop = false;
      // 
      // lblTelo2
      // 
      this.lblTelo2.AutoSize = true;
      this.lblTelo2.Location = new System.Drawing.Point(18, 26);
      this.lblTelo2.Name = "lblTelo2";
      this.lblTelo2.Size = new System.Drawing.Size(72, 13);
      this.lblTelo2.TabIndex = 0;
      this.lblTelo2.Text = "Tělo kamene:";
      // 
      // gbHraciDeska
      // 
      this.gbHraciDeska.Controls.Add(this.btnHraciDeskaObrys);
      this.gbHraciDeska.Controls.Add(this.txtHraciDeskaObrys);
      this.gbHraciDeska.Controls.Add(this.lblHraciDeska2);
      this.gbHraciDeska.Controls.Add(this.btnHraciDeska2);
      this.gbHraciDeska.Controls.Add(this.txtHraciDeska2);
      this.gbHraciDeska.Controls.Add(this.btnHraciDeska1);
      this.gbHraciDeska.Controls.Add(this.txtHraciDeska1);
      this.gbHraciDeska.Controls.Add(this.lblHraciDeska1);
      this.gbHraciDeska.Location = new System.Drawing.Point(12, 108);
      this.gbHraciDeska.Name = "gbHraciDeska";
      this.gbHraciDeska.Size = new System.Drawing.Size(238, 126);
      this.gbHraciDeska.TabIndex = 6;
      this.gbHraciDeska.TabStop = false;
      this.gbHraciDeska.Text = "Hrací deska";
      // 
      // btnHraciDeskaObrys
      // 
      this.btnHraciDeskaObrys.Location = new System.Drawing.Point(168, 88);
      this.btnHraciDeskaObrys.Name = "btnHraciDeskaObrys";
      this.btnHraciDeskaObrys.Size = new System.Drawing.Size(50, 23);
      this.btnHraciDeskaObrys.TabIndex = 8;
      this.btnHraciDeskaObrys.Text = "Změnit";
      this.btnHraciDeskaObrys.UseVisualStyleBackColor = true;
      this.btnHraciDeskaObrys.Click += new System.EventHandler(this.btnHraciDeskaObrys_Click);
      // 
      // txtHraciDeskaObrys
      // 
      this.txtHraciDeskaObrys.Cursor = System.Windows.Forms.Cursors.Default;
      this.txtHraciDeskaObrys.Enabled = false;
      this.txtHraciDeskaObrys.Location = new System.Drawing.Point(118, 88);
      this.txtHraciDeskaObrys.Multiline = true;
      this.txtHraciDeskaObrys.Name = "txtHraciDeskaObrys";
      this.txtHraciDeskaObrys.ReadOnly = true;
      this.txtHraciDeskaObrys.Size = new System.Drawing.Size(23, 23);
      this.txtHraciDeskaObrys.TabIndex = 7;
      this.txtHraciDeskaObrys.TabStop = false;
      // 
      // lblHraciDeska2
      // 
      this.lblHraciDeska2.AutoSize = true;
      this.lblHraciDeska2.Location = new System.Drawing.Point(18, 93);
      this.lblHraciDeska2.Name = "lblHraciDeska2";
      this.lblHraciDeska2.Size = new System.Drawing.Size(76, 13);
      this.lblHraciDeska2.TabIndex = 6;
      this.lblHraciDeska2.Text = "Obrys políček:";
      // 
      // btnHraciDeska2
      // 
      this.btnHraciDeska2.Location = new System.Drawing.Point(168, 50);
      this.btnHraciDeska2.Name = "btnHraciDeska2";
      this.btnHraciDeska2.Size = new System.Drawing.Size(50, 23);
      this.btnHraciDeska2.TabIndex = 5;
      this.btnHraciDeska2.Text = "Změnit";
      this.btnHraciDeska2.UseVisualStyleBackColor = true;
      this.btnHraciDeska2.Click += new System.EventHandler(this.btnHraciDeska2_Click);
      // 
      // txtHraciDeska2
      // 
      this.txtHraciDeska2.Cursor = System.Windows.Forms.Cursors.Default;
      this.txtHraciDeska2.Enabled = false;
      this.txtHraciDeska2.Location = new System.Drawing.Point(118, 50);
      this.txtHraciDeska2.Multiline = true;
      this.txtHraciDeska2.Name = "txtHraciDeska2";
      this.txtHraciDeska2.ReadOnly = true;
      this.txtHraciDeska2.Size = new System.Drawing.Size(23, 23);
      this.txtHraciDeska2.TabIndex = 4;
      this.txtHraciDeska2.TabStop = false;
      // 
      // btnHraciDeska1
      // 
      this.btnHraciDeska1.Location = new System.Drawing.Point(168, 21);
      this.btnHraciDeska1.Name = "btnHraciDeska1";
      this.btnHraciDeska1.Size = new System.Drawing.Size(50, 23);
      this.btnHraciDeska1.TabIndex = 2;
      this.btnHraciDeska1.Text = "Změnit";
      this.btnHraciDeska1.UseVisualStyleBackColor = true;
      this.btnHraciDeska1.Click += new System.EventHandler(this.btnHraciDeska1_Click);
      // 
      // txtHraciDeska1
      // 
      this.txtHraciDeska1.Cursor = System.Windows.Forms.Cursors.Default;
      this.txtHraciDeska1.Enabled = false;
      this.txtHraciDeska1.Location = new System.Drawing.Point(118, 21);
      this.txtHraciDeska1.Multiline = true;
      this.txtHraciDeska1.Name = "txtHraciDeska1";
      this.txtHraciDeska1.ReadOnly = true;
      this.txtHraciDeska1.Size = new System.Drawing.Size(23, 23);
      this.txtHraciDeska1.TabIndex = 1;
      this.txtHraciDeska1.TabStop = false;
      // 
      // lblHraciDeska1
      // 
      this.lblHraciDeska1.AutoSize = true;
      this.lblHraciDeska1.Location = new System.Drawing.Point(18, 26);
      this.lblHraciDeska1.Name = "lblHraciDeska1";
      this.lblHraciDeska1.Size = new System.Drawing.Size(76, 13);
      this.lblHraciDeska1.TabIndex = 0;
      this.lblHraciDeska1.Text = "Barvy políček:";
      // 
      // btnOk
      // 
      this.btnOk.DialogResult = System.Windows.Forms.DialogResult.OK;
      this.btnOk.Location = new System.Drawing.Point(259, 236);
      this.btnOk.Name = "btnOk";
      this.btnOk.Size = new System.Drawing.Size(75, 23);
      this.btnOk.TabIndex = 7;
      this.btnOk.Text = "OK";
      this.btnOk.UseVisualStyleBackColor = true;
      this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
      // 
      // btnCancel
      // 
      this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnCancel.Location = new System.Drawing.Point(340, 236);
      this.btnCancel.Name = "btnCancel";
      this.btnCancel.Size = new System.Drawing.Size(75, 23);
      this.btnCancel.TabIndex = 8;
      this.btnCancel.Text = "Cancel";
      this.btnCancel.UseVisualStyleBackColor = true;
      // 
      // btnDefault
      // 
      this.btnDefault.Location = new System.Drawing.Point(421, 236);
      this.btnDefault.Name = "btnDefault";
      this.btnDefault.Size = new System.Drawing.Size(75, 23);
      this.btnDefault.TabIndex = 9;
      this.btnDefault.Text = "Výchozí";
      this.btnDefault.UseVisualStyleBackColor = true;
      this.btnDefault.Click += new System.EventHandler(this.btnDefault_Click);
      // 
      // NastaveniVzhledu
      // 
      this.AcceptButton = this.btnOk;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnCancel;
      this.ClientSize = new System.Drawing.Size(508, 271);
      this.Controls.Add(this.btnDefault);
      this.Controls.Add(this.btnCancel);
      this.Controls.Add(this.btnOk);
      this.Controls.Add(this.gbHraciDeska);
      this.Controls.Add(this.gbHrac2);
      this.Controls.Add(this.gbHrac1);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.MaximizeBox = false;
      this.MinimizeBox = false;
      this.Name = "NastaveniVzhledu";
      this.ShowIcon = false;
      this.ShowInTaskbar = false;
      this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
      this.Text = "Nastavení vzhledu";
      this.Load += new System.EventHandler(this.NastaveniVzhledu_Load);
      this.gbHrac1.ResumeLayout(false);
      this.gbHrac1.PerformLayout();
      this.gbHrac2.ResumeLayout(false);
      this.gbHrac2.PerformLayout();
      this.gbHraciDeska.ResumeLayout(false);
      this.gbHraciDeska.PerformLayout();
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.GroupBox gbHrac1;
    private System.Windows.Forms.Label lblTelo1;
    private System.Windows.Forms.Button btnTelo1;
    private System.Windows.Forms.TextBox txtTelo1;
    private System.Windows.Forms.Button btnOkraj1;
    private System.Windows.Forms.TextBox txtOkraj1;
    private System.Windows.Forms.Label lblOkraj1;
    private System.Windows.Forms.GroupBox gbHrac2;
    private System.Windows.Forms.Button btnOkraj2;
    private System.Windows.Forms.TextBox txtOkraj2;
    private System.Windows.Forms.Label lblOkraj2;
    private System.Windows.Forms.Button btnTelo2;
    private System.Windows.Forms.TextBox txtTelo2;
    private System.Windows.Forms.Label lblTelo2;
    private System.Windows.Forms.GroupBox gbHraciDeska;
    private System.Windows.Forms.Button btnHraciDeska2;
    private System.Windows.Forms.TextBox txtHraciDeska2;
    private System.Windows.Forms.Button btnHraciDeska1;
    private System.Windows.Forms.TextBox txtHraciDeska1;
    private System.Windows.Forms.Label lblHraciDeska1;
    private System.Windows.Forms.Button btnHraciDeskaObrys;
    private System.Windows.Forms.TextBox txtHraciDeskaObrys;
    private System.Windows.Forms.Label lblHraciDeska2;
    private System.Windows.Forms.Button btnOk;
    private System.Windows.Forms.Button btnCancel;
    private System.Windows.Forms.Button btnDefault;
  }
}