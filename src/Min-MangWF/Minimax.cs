﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Min_Mang
{
  public class Minimax
  {

    // Vraci random z vyslednych moznych tahu
    public static Tah Random(List<Tah> poleTahu) {
      Random rand = new Random();
      int randValue = rand.Next(0, poleTahu.Count());
      return poleTahu[randValue];
    }

    public static Tah WorkMinimax(StavHraciDesky deska, HerniBarva barva, int hloubka, bool minOrMax) {
      bool testJedineho = deska.SpocitejKameny(barva) == 1;
      Tah[] tahy;

      if (testJedineho) {
        tahy = Rozhodci.DejMnozinuVsechMoznychTahuPreskoky(deska, barva);
      }
      else {
        tahy = Rozhodci.DejMnozinuVsechMoznychTahu(deska, barva);
      }

      foreach (Tah t in tahy) {
        // Provádění tahů a vyhazování figurek
        StavHraciDesky aktDeska = new StavHraciDesky(deska);
        aktDeska.ProvedTah(t);

        int[] vyhozeneIndexy;
        if (testJedineho) {
          vyhozeneIndexy = Rozhodci.DejIndexyVyhozeni(aktDeska, t);
          List<int> tempVyh = vyhozeneIndexy.ToList();
          foreach (int i in Rozhodci.DejIndexyVyhozeniPreskoky(aktDeska,t)) {
            tempVyh.Add(i);
          }
          vyhozeneIndexy = tempVyh.ToArray();
        }
        else {
          vyhozeneIndexy = Rozhodci.DejIndexyVyhozeni(aktDeska, t);
        }

        aktDeska.VyhodKameny(vyhozeneIndexy);
        if (hloubka > 1)
          t.Ohodnoceni = WorkMinimax(aktDeska,
            (barva == HerniBarva.BILY ? HerniBarva.CERNY : HerniBarva.BILY),
            hloubka - 1, (minOrMax ? false : true)).Ohodnoceni;
        else
          Ohodnot(aktDeska, t, barva);
      }
      int value;
      if (minOrMax) {
        if (tahy.Length == 0) {
          Tah t = new Tah(0, 0);
          t.Ohodnoceni = -1;
          return t;
        }
        else
          value = tahy.Max<Tah>(i => i.Ohodnoceni);
      }
      else {
        if (tahy.Length == 0) {
          Tah t = new Tah(0, 0);
          t.Ohodnoceni = 100;
          return t;
        }
        else
          value = tahy.Min<Tah>(i => i.Ohodnoceni);
      }
      List<Tah> temp = new List<Tah>();
      foreach (Tah t in tahy) {
        if (t.Ohodnoceni == value)
          temp.Add(t);
      }
      return Random(temp);
    }


    private static void Ohodnot(StavHraciDesky deska, Tah tah, HerniBarva hrac) {
      int pocetBilych = deska.SpocitejKameny(HerniBarva.BILY);
      int pocetCernych = deska.SpocitejKameny(HerniBarva.CERNY);

      if (hrac == HerniBarva.BILY)
        tah.Ohodnoceni = 15 + pocetBilych - pocetCernych;
      else
        tah.Ohodnoceni = 15 + pocetCernych - pocetBilych;
    }

  }
}
