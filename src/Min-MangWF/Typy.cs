﻿namespace Min_Mang {

  /// <summary>
  /// Barva hráče, polí na hrací desce, když je pole volné, bude NULL
  /// </summary>
  public enum HerniBarva { BILY, CERNY, PRAZDNY };

  public enum TypHrace { Clovek, Pocitac };
  
  public enum Obtiznost { Lehka = 1, Stredni = 2, Tezka = 3 };

  public enum InfoText { Uvitani, Hraje1, Hraje2, Vyhral1, Vyhral2, Pozastaveno, Spusteno };

  public class Tah {

    /// <summary>
    /// Konstruktor tahu
    /// </summary>
    /// <param name="odkud">Index pole, ze kterého se táhne</param>
    /// <param name="kam">Index pole, na které se táhne</param>
    public Tah(int odkud, int kam) {
      this.Odkud = odkud;
      this.Kam = kam;
    }

    public int Odkud { get; set; }
    public int Kam { get; set; }
    public int Ohodnoceni { get; set; }
    public int[] VyhozeneIndexy { get; set; }

    public override string ToString() {
      string text = "";
      text += prevedNaSouradnice(this.Odkud);
      text += "  -->  ";
      text += prevedNaSouradnice(this.Kam);
      return text;
    }

    public static string prevedNaSouradnice (int index) {
      string text="";

      switch (index % 9){
        case 0:
          text+="A";
          break;
        case 1:
          text+="B";
          break;
        case 2:
          text += "C";
          break;
        case 3:
          text+="D";
          break;
        case 4:
          text+="E";
          break;
        case 5:
          text+="F";
          break;
        case 6:
          text+="G";
          break;
        case 7:
          text+="H";
          break;
        case 8:
          text += "I";
          break;
      }

      switch (index / 9) {
        case 0:
          text += "9";
          break;
        case 1:
          text += "8";
          break;
        case 2:
          text += "7";
          break;
        case 3:
          text += "6";
          break;
        case 4:
          text += "5";
          break;
        case 5:
          text += "4";
          break;
        case 6:
          text += "3";
          break;
        case 7:
          text += "2";
          break;
        case 8:
          text += "1";
          break;
      }

      return text;
    }
  
  }
}