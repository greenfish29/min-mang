﻿using System;
using System.Collections.Generic;

namespace Min_Mang
{
  public class Rozhodci
  {

    /// <summary>
    /// Zjistí, zda je dané políčko na hrací desce
    /// </summary>
    /// <param name="index">Index políčka, které nás zajímá</param>
    private static bool JeTahNaDesce(int index) {
      return (index <= 80 && index >= 0);
    }

    /// <summary>
    /// Zjistí, zda je tah veden na sousední pole dle pravidel hry
    /// </summary>
    /// <param name="odkud">Index pole odkud táhneme kamenem</param>
    /// <param name="kam">Index pole kam táhneme kamenem</param>
    private static bool JeTahSousedni(int odkud, int kam) {
      return ((odkud % 9 == kam % 9 || odkud / 9 == kam / 9) && (Math.Abs(odkud - kam) == 1 || Math.Abs(odkud - kam) == 9));
    }

    /// <summary>
    /// Zjistí, zda je pole kam táhneme volné nebo obsazené jiným kamenem
    /// </summary>
    /// <param name="deska">Stav hrací desky, nad kterou kontrola tahu probíhá</param>
    /// <param name="kam">Index pole kam táhneme kamenem</param>
    private static bool JePoleVolne(StavHraciDesky deska, int kam) {
      if (JeTahNaDesce(kam))
        return (deska.VratHodnotuPole(kam) == HerniBarva.PRAZDNY);
      else
        return false;
    }


    public static bool JePreskok(StavHraciDesky deska, Tah t) {
      if ((t.Odkud % 9 == t.Kam % 9 || t.Odkud / 9 == t.Kam / 9) &&
          (Math.Abs(t.Odkud - t.Kam) == 2 || Math.Abs(t.Odkud - t.Kam) == 18) &&
          (!JePoleVolne(deska,(t.Odkud+t.Kam)/2)))
        return true;
      else
        return false;
    }


    /// <summary>
    /// Vrací množinu možných tahů pro určitý kámen na určité desce
    /// </summary>
    /// <param name="deska">Deska, na které se hledají možné tahy</param>
    /// <param name="odkud">Index pole, odkud tah začal (kontrolují se všechny 4 směry)</param>
    /// <returns></returns>
    public static Tah[] DejMnozinuMoznychTahu(StavHraciDesky deska, int odkud) {
      List<Tah> temp = new List<Tah>();

      Tah novy = new Tah(odkud, odkud + 9);
      if (JeTahValidni(deska, novy))
        temp.Add(novy);

      novy = new Tah(odkud, odkud - 9);
      if (JeTahValidni(deska, novy))
        temp.Add(novy);

      novy = new Tah(odkud, odkud - 1);
      if (JeTahValidni(deska, novy))
        temp.Add(novy);

      novy = new Tah(odkud, odkud + 1);
      if (JeTahValidni(deska, novy))
        temp.Add(novy);

      return temp.ToArray();
    }

    public static Tah[] DejMnozinuMoznychTahuPreskoky(StavHraciDesky deska, int odkud) {
      List<Tah> temp = new List<Tah>();

      Tah novy = new Tah(odkud, odkud + 9);
      Tah t = new Tah(odkud, odkud + 18);
      if (JeTahValidni(deska, novy))
        temp.Add(novy);
      else if (JeTahValidniPreskoky(deska, t) && !JePoleVolne(deska, odkud + 9) && deska.VratHodnotuPole(odkud)!= deska.VratHodnotuPole(novy.Kam)) {
        if (JePreskok(deska, t))
          temp.Add(t);
      }

      novy = new Tah(odkud, odkud - 9);
      t = new Tah(odkud, odkud - 18);
      if (JeTahValidni(deska, novy))
        temp.Add(novy);
      else if (JeTahValidniPreskoky(deska, t) && !JePoleVolne(deska, odkud - 9) && deska.VratHodnotuPole(odkud) != deska.VratHodnotuPole(novy.Kam)) {
        if (JePreskok(deska, t))
          temp.Add(t);
      }

      novy = new Tah(odkud, odkud - 1);
      t = new Tah(odkud, odkud - 2);
      if (JeTahValidni(deska, novy))
        temp.Add(novy);
      else if (JeTahValidniPreskoky(deska, t) && !JePoleVolne(deska, odkud - 1) && deska.VratHodnotuPole(odkud) != deska.VratHodnotuPole(novy.Kam)) {
        if (JePreskok(deska, t))
          temp.Add(t);
      }

      novy = new Tah(odkud, odkud + 1);
      t = new Tah(odkud, odkud + 2);
      if (JeTahValidni(deska, novy))
        temp.Add(novy);
      else if (JeTahValidniPreskoky(deska, t) && !JePoleVolne(deska, odkud + 1) && deska.VratHodnotuPole(odkud) != deska.VratHodnotuPole(novy.Kam)) {
        if (JePreskok(deska,t))
          temp.Add(t);
      }

      return temp.ToArray();
    }


    /// <summary>
    /// Vrací množinu možných tahů pro všechny kameny stejné barvy
    /// </summary>
    /// <param name="deska">Deska, na které se hledají možné tahy</param>
    /// <param name="barva">Barva kamenů, pro které se tahy hledají</param>
    /// <returns></returns>
    public static Tah[] DejMnozinuVsechMoznychTahu(StavHraciDesky deska, HerniBarva barva) {
      Tah[] temp;
      List<Tah> result = new List<Tah>();

      for (int i = 0; i < 81; i++) {
        if (deska.VratHodnotuPole(i) == barva) {
          temp = DejMnozinuMoznychTahu(deska, i);
          foreach (Tah t in temp) {
            result.Add(t);
          }
        }
      }

      return result.ToArray();
    }


    public static Tah[] DejMnozinuVsechMoznychTahuPreskoky (StavHraciDesky deska, HerniBarva barva) {
      Tah[] temp;
      List<Tah> result = new List<Tah>();

      for (int i = 0; i < 81; i++) {
        if (deska.VratHodnotuPole(i) == barva) {
          temp = DejMnozinuMoznychTahuPreskoky(deska, i);
          foreach (Tah t in temp) {
            result.Add(t);
          }
        }
      }

      return result.ToArray();
    }


    /// <summary>
    /// Zjistí, zda je tah podle pravidel hry s využitím statických metod třídy Rozhodčí
    /// </summary>
    /// <param name="stav">Stav hrací desky, nad kterou kontrola tahu probíhá</param>
    /// <param name="tah">Tah, který se má kontrolovat</param>
    public static bool JeTahValidni(StavHraciDesky stav, Tah tah) {
      int odkud = tah.Odkud;
      int kam = tah.Kam;
      return ((odkud != kam) && JeTahNaDesce(kam) && JeTahNaDesce(odkud) && JeTahSousedni(odkud, kam)
               && JePoleVolne(stav, kam));
    }

    public static bool JeTahValidniPreskoky(StavHraciDesky stav, Tah tah) {
      int odkud = tah.Odkud;
      int kam = tah.Kam;
      return ((odkud != kam) && JeTahNaDesce(kam) && JeTahNaDesce(odkud) && JePoleVolne(stav, kam));
              //&& JePreskok(stav, tah));
    }


    /// <summary>
    /// Zjišťuje, které kameny na kterých pozicích se mají vyhodit
    /// </summary>
    /// <param name="stav">Aktuální stav hrací desky</param>
    /// <param name="t">Prováďený tah</param>
    /// <returns>Pole obsahující indexy, na kterých budou kameny vyhozeny</returns>
    public static int[] DejIndexyVyhozeni(StavHraciDesky stav, Tah t) {
      List<int> seznamIndexu = new List<int>();
      Tah[] pole;

      int index = t.Kam;

      // Pokud nemá sousední kámen žádné možné tahy, tak je kandidát na vyhození
      if (JeTahSousedni(index, index + 1) && JeTahNaDesce(index + 1) && stav.VratHodnotuPole(index) != stav.VratHodnotuPole(index + 1)) {
        pole = DejMnozinuMoznychTahu(stav, index + 1);
        if (pole.Length == 0)
          seznamIndexu.Add(index + 1);
      }

      if (JeTahSousedni(index, index - 1) && JeTahNaDesce(index - 1) && stav.VratHodnotuPole(index) != stav.VratHodnotuPole(index - 1)) {
        pole = DejMnozinuMoznychTahu(stav, index - 1);
        if (pole.Length == 0)
          seznamIndexu.Add(index - 1);
      }

      if (JeTahNaDesce(index + 9) && stav.VratHodnotuPole(index) != stav.VratHodnotuPole(index + 9)) {
        pole = DejMnozinuMoznychTahu(stav, index + 9);
        if (pole.Length == 0)
          seznamIndexu.Add(index + 9);
      }

      if (JeTahNaDesce(index - 9) && stav.VratHodnotuPole(index) != stav.VratHodnotuPole(index - 9)) {
        pole = DejMnozinuMoznychTahu(stav, index - 9);
        if (pole.Length == 0)
          seznamIndexu.Add(index - 9);
      }

      // vyradi zaporne indexy, ktere vznikaji v horni a spodni rade
      for (int i = 0; i < seznamIndexu.Count; i++) {
        if ((seznamIndexu[i] < 0) || (seznamIndexu[i] > 80)) {
          seznamIndexu.Remove(seznamIndexu[i]);
          i--;
        }
      }
      // vyradi vznikle indexy vyhozeni, na kterych neni hraci kamen

      for (int i = 0; i<seznamIndexu.Count;i++) {
        if (stav.VratHodnotuPole(seznamIndexu[i]) == HerniBarva.PRAZDNY) {
          seznamIndexu.Remove(seznamIndexu[i]);
          i--;
        }
      }

      return seznamIndexu.ToArray();
    }


    public static int[] DejIndexyVyhozeniPreskoky(StavHraciDesky stav, Tah t) {
      List<int> seznamIndexu = new List<int>();

      if (!JeTahSousedni(t.Odkud, t.Kam)) {
        int index = (t.Odkud + t.Kam) / 2;
        seznamIndexu.Add(index);
      }

      return seznamIndexu.ToArray();
    }


    public static bool KonecHry(StavHraciDesky deska) {
      if (deska.SpocitejKameny(HerniBarva.BILY) == 0 || deska.SpocitejKameny(HerniBarva.CERNY) == 0)
        return true;
      else
        return false;
    }

  }
}
