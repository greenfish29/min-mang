﻿using System.Windows.Forms;
using System.Threading;
using System.Collections.Generic;

namespace Min_Mang
{

  public delegate void ZmenaStavuHandler();

  public class Manazer
  {

    GUI grafika = new GUI();
    Hrac bily;
    Hrac cerny;
    LidskyHrac clovek = new LidskyHrac();
    PocitacovyHrac pc = new PocitacovyHrac();
    HerniBarva kdoTahne;
    public EventWaitHandle ewh = new AutoResetEvent(false);
    bool konecHry = false;
    int obtiznostH1 = 1;
    int obtiznostH2 = 1;
    Thread herniSmycka;


    public Manazer() {
      grafika.StartNovaHra += new ZmenaStavuHandler(StartNewGame);
      grafika.ZmenaNastaveniHracu += new ZmenaStavuHandler(ZmenaHracu);
      grafika.Undo += new ZmenaStavuHandler(Undo);
      grafika.Rendo += new ZmenaStavuHandler(Redo);
      grafika.BestMove += new ZmenaStavuHandler(BestMove);
      grafika.ObnovHru += new ZmenaStavuHandler(ObnovHru);
      grafika.Loading += new ZmenaStavuHandler(ProbehlLoad);
      Application.Run(grafika);
      Application.Exit();
    }


    /// <summary>
    /// Pomocná funkce, nastaví hráče a spustí herní vlákno
    /// </summary>
    public void ZmenaHracu() {
      if (grafika.typHrace1 == TypHrace.Pocitac) {
        bily = pc;
        obtiznostH1 = (int)grafika.obtiznostH1;
      }
      else {
        bily = clovek;
        obtiznostH1 = -1; // nastaveni na nesmysl, nema stejne vliv na vypocet
      }

      if (grafika.typHrace2 == TypHrace.Pocitac) {
        cerny = pc;
        obtiznostH2 = (int)grafika.obtiznostH2;
      }
      else {
        cerny = clovek;
        obtiznostH2 = -1;
      }

      if (herniSmycka != null) {
        herniSmycka.Abort();
        herniSmycka = new Thread(PrubehHry);
        herniSmycka.Name = "Dalsi herni smycka";
        grafika.NastavEWH(ewh);
        herniSmycka.Start();
      }
      else {
        herniSmycka = new Thread(PrubehHry);
        herniSmycka.Name = "Prvni herni smycka";
        grafika.NastavEWH(ewh);
        herniSmycka.Start();
      }
    }


    /// <summary>
    /// Po spuštění nové hry provede potřebná nastavení a spustí herní smyčku
    /// </summary>
    public void StartNewGame() {
      grafika.aktualniStavDesky = new StavHraciDesky();
      grafika.aktualizujSouradniceFigurek(true);
      //grafika.NastavEWH(ewh);
      kdoTahne = HerniBarva.BILY;
      aktualniPozice = 0;
      grafika.AktualniPozice = aktualniPozice;
      // nastaví při každé nové hře na defaultní hodnoty
      grafika.NastavKomuPovolitTahy(kdoTahne);
      // Nastaví typy hráčů a obtížnost hráčů
      ZmenaHracu();
    }



    /// <summary>
    /// Hlavní herní smyčka, řídící průběh celé hry
    /// </summary>
    public void PrubehHry() {

      while (!konecHry && !Rozhodci.KonecHry(grafika.aktualniStavDesky)) {

        konecHry = grafika.formClosed;
        if (!konecHry) {


          if (grafika.komuPovolitTahy == HerniBarva.BILY) {
            if (bily == clovek && !grafika.IsBtnPauseOn)
              grafika.NastavBestMoveEnabled(true);
            else
              grafika.NastavBestMoveEnabled(false);

            grafika.SetInfoText(InfoText.Hraje1);
            bily.vratTah(grafika, kdoTahne, ewh, obtiznostH1);
          }
          else {
            if (cerny == clovek && !grafika.IsBtnPauseOn)
              grafika.NastavBestMoveEnabled(true);
            else
              grafika.NastavBestMoveEnabled(false);

            grafika.SetInfoText(InfoText.Hraje2);
            cerny.vratTah(grafika, kdoTahne, ewh, obtiznostH2);

          }


          if (!grafika.IsPaused) {
            if (grafika.provadenyTah != null &&
              (kdoTahne == grafika.aktualniStavDesky.VratHodnotuPole(grafika.provadenyTah.Odkud))) {

              bool testJedineho = grafika.aktualniStavDesky.SpocitejKameny(kdoTahne) == 1;
              bool testTahu;

              if (testJedineho) {
                testTahu = false;
                foreach (Tah t in Rozhodci.DejMnozinuMoznychTahuPreskoky(grafika.aktualniStavDesky, grafika.provadenyTah.Odkud)) {
                  if (grafika.provadenyTah.Kam == t.Kam) {
                    testTahu = true;
                    break;
                  }
                }
              }
              else {
                testTahu = Rozhodci.JeTahValidni(grafika.aktualniStavDesky, grafika.provadenyTah);
              }

              if (testTahu) {
                grafika.NastavPovoleneTahnuti(false);
                grafika.aktualniStavDesky = grafika.aktualniStavDesky.ProvedTah(grafika.provadenyTah);


                int[] vyhozeneIndexy;
                if (testJedineho) {
                  vyhozeneIndexy = Rozhodci.DejIndexyVyhozeni(grafika.aktualniStavDesky, grafika.provadenyTah);
                  List<int> tempVyh = new List<int>();
                  foreach (int i in vyhozeneIndexy) {
                    tempVyh.Add(i);
                  }
                  foreach (int i in Rozhodci.DejIndexyVyhozeniPreskoky(grafika.aktualniStavDesky, grafika.provadenyTah)) {
                    tempVyh.Add(i);
                  }
                  vyhozeneIndexy = tempVyh.ToArray();
                }
                else {
                  vyhozeneIndexy = Rozhodci.DejIndexyVyhozeni(grafika.aktualniStavDesky, grafika.provadenyTah);
                }


                // promaže historii, pokud se ji pokusíme přepisovat
                if (grafika.aktualniStavDesky.HistorieTahu.Count != aktualniPozice) {
                  for (int i = grafika.aktualniStavDesky.HistorieTahu.Count - 1; i >= aktualniPozice; i--) {
                    grafika.aktualniStavDesky.HistorieTahu.RemoveAt(i);
                  }
                }

                // pridani do tahu kvuli historii
                grafika.provadenyTah.VyhozeneIndexy = vyhozeneIndexy;
                aktualniPozice += 1;
                grafika.AktualniPozice = aktualniPozice;
                grafika.aktualniStavDesky.PridejTahDoHistorie(grafika.provadenyTah);

                grafika.aktualniStavDesky.VyhodKameny(vyhozeneIndexy);

                kdoTahne = (kdoTahne == HerniBarva.BILY) ? HerniBarva.CERNY : HerniBarva.BILY;

                grafika.NastavKomuPovolitTahy(kdoTahne);
                grafika.historie = true;

                grafika.aktualizujSouradniceFigurek(true);


                if (Rozhodci.KonecHry(grafika.aktualniStavDesky)) {

                  grafika.IsGameRepeated = true;
                  if (kdoTahne == HerniBarva.BILY)
                    grafika.SetInfoText(InfoText.Vyhral2);
                  else
                    grafika.SetInfoText(InfoText.Vyhral1);

                  MessageBox.Show("Vyhrál " + (kdoTahne == HerniBarva.BILY ? "Hráč 2" : "Hráč 1"), "Min-Mang",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
              }
              else {
                grafika.aktualizujSouradniceFigurek(true);
              }
            }
            else {
              grafika.aktualizujSouradniceFigurek(true);
            }
          }
          else {
            ewh.WaitOne();
          }
        }
      }
    }

    int aktualniPozice = 0;

    private void Undo() {
      StavHraciDesky deska = grafika.aktualniStavDesky;
      aktualniPozice -= 1;
      Tah t = deska.HistorieTahu[aktualniPozice];
      grafika.AktualniPozice = aktualniPozice;
      kdoTahne = deska.VratHodnotuPole(t.Kam);
      grafika.NastavKomuPovolitTahy(kdoTahne);
      deska = deska.ReverseTah(t);
      deska.ReverseVyhodKameny(t.VyhozeneIndexy, kdoTahne);
      //deska.HistorieTahu.Remove(t);
      if (grafika.aktualniStavDesky.HistorieTahu.Count != aktualniPozice) {
        grafika.SmazatHistorii = true;
      }
      else {
        grafika.SmazatHistorii = false;
      }
      grafika.aktualizujSouradniceFigurek(true);
    }

    private void Redo() {
      StavHraciDesky deska = grafika.aktualniStavDesky;
      Tah t = deska.HistorieTahu[aktualniPozice];
      aktualniPozice += 1;
      grafika.AktualniPozice = aktualniPozice;
      kdoTahne = deska.VratHodnotuPole(t.Odkud) == HerniBarva.BILY ? HerniBarva.CERNY : HerniBarva.BILY;
      grafika.NastavKomuPovolitTahy(kdoTahne);
      deska = deska.ProvedTah(t);
      deska.VyhodKameny(t.VyhozeneIndexy);
      //deska.HistorieTahu.Add(t);
      if (grafika.aktualniStavDesky.HistorieTahu.Count != aktualniPozice) {
        grafika.SmazatHistorii = true;
      }
      else {
        grafika.SmazatHistorii = false;
      }
      grafika.aktualizujSouradniceFigurek(true);
    }

    /// <summary>
    /// Nápověda nejlepšího tahu
    /// </summary>
    private void BestMove() {
      grafika.provadenyTah = Minimax.WorkMinimax(grafika.aktualniStavDesky, kdoTahne, 3, true);
    }

    private void ObnovHru() {
      konecHry = false;
      grafika.IsGameRepeated = false;
      herniSmycka = new Thread(PrubehHry);
      herniSmycka.Name = "Prvni herni smycka";
      herniSmycka.Start();
    }

    private void ProbehlLoad() {
      aktualniPozice = grafika.AktualniPozice;
      grafika.aktualizujSouradniceFigurek(true);
      //grafika.aktualizujSouradniceFigurek(true);
      if (!grafika.LoadWithError) {
        kdoTahne = grafika.komuPovolitTahy;
        grafika.LoadWithError = false;
        //grafika.aktualizujSouradniceFigurek(true);
        ZmenaHracu();
      }
    }

  }
}
