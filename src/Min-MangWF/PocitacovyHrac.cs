﻿using System.Threading;

namespace Min_Mang
{
  class PocitacovyHrac : Hrac
  {

    public override void vratTah(GUI grafika, HerniBarva kdoTahne, EventWaitHandle value, int hloubka) {
      Thread.Sleep(1000);
      grafika.provadenyTah = Minimax.WorkMinimax(grafika.aktualniStavDesky, kdoTahne, hloubka, true);
    }

  }
}